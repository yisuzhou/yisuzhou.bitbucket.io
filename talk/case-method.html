<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>教师教学方法与教学模式创新</title>
    <meta charset="utf-8" />
    <meta name="author" content="周忆粟教授" />
    <script src="libs/header-attrs-2.23/header-attrs.js"></script>
    <link rel="stylesheet" href="libs/remark-css/default.css" type="text/css" />
    <link rel="stylesheet" href="libs/remark-css/metropolis.css" type="text/css" />
    <link rel="stylesheet" href="libs/remark-css/metropolis-fonts.css" type="text/css" />
    <link rel="stylesheet" href="libs/remark-css/metropolis-extra.css" type="text/css" />
    <link rel="stylesheet" href="libs/remark-css/metropolis-extra-seahorse.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# 教师教学方法与教学模式创新
]
.subtitle[
## 案例教学与课堂管理
]
.author[
### 周忆粟教授
]
.institute[
### 澳门大学教育学院
]
.date[
### 2023-08
]

---



# 今天的课件

可在任何设备上扫码打开：https://bit.ly/3Ps7agF

![:wscale 65%](./asset/case-method-qr-code.png)

---

# 引子 Introduction
## 什么是案例法？What is Case Method

案例法既是一种**研究方法**（research method），也是一种**教学法**（pedagogical approach），它提供了一种独特的帮助我们理解社会世界的途径。

我们今天会分两个部分来谈案例法的教学：

1. 作为一种研究方法
  + 案例是什么？
  + 案例能告诉我们些什么？
  + 我们究竟需要多少案例？

2. 作为一种教学法
  + 案例教学带给我们什么？
  + 案例教学过程中的课堂管理


---

### 每个人接触案例法的契机都不同

我是一名社会学家，在教育学院工作。我对案例法的使用源自一位社会学家的自觉：解释周遭的社会世界。我们的社会世界充满了各式各样的行动者，他们彼此之间互动，和周遭环境互动。作为研究者，我感兴趣的是：他们为什么这样行动？

而对于我们社会学家而言，.orange[案例法是我们的常规研究方法之一]。不过在社会科学里，其实没有一种研究方法叫「案例法」（case method），当我们说案例的时候，我们一般指的是历史方法（history）和小样本比较（Small-*N* Comparison）的结合。

---

## 社会科学家的研究方法

在所有社会科学里，我们的研究方法的光谱上有两个极端：

--

**历史方法**（History）：历史叙事是一种方法论传统。历史著作大多是描述性的，分析特定时间、地点的事态究竟发生了什么。不过，历史学家经常用.cyan[叙事]的方式提出特定问题：最常见的是，.violet[为什么某个事件会发生？]历史学家使用的方法核心来自「阅读档案」（即过去发生的事）。通过档案，历史学家获得事件为何发生的知识。

--

**标准因果分析**（Standard Casual Analysis，SCA）：需要大量案例、测量案例的各个方面，使用.cyan[统计模型]来对这些测量之间的关系进行推断，然后使用推理来认识从数据中观察到的.violet[相关模式的肇因]（causes）。SCA其实对案例的具体细节不感兴趣，我们希望的是从大量数据中找到统计规律──对于量化分析者而言，反复出现的社会现象就代表了规律。

---

**小样本比较**则是介于历史和统计学分析之间的一条中间道路。我们既可以对单个案例的历史或当下实际进行详细分析，也可以对案例进行统计学分析。典型的小样本比较研究少量案例，从几个到十几个不等。案例可以有很多种类：课堂、备课小组、学校、管理机关，或者其他任何形式的社会组织（social organization）。

--

小样本的灵活性体现在其收集数据的方式多样（这点承继自历史方法，相比之下，SCA只能运用统计数据）：

- 可以让研究自己深入研究群体，成为他们中的一员，用观察的方式收集（.violet[民族志]）。
- 可以通过结构化的提问（.violet[问卷]或.violet[访谈]）。
- 可以通过使用某个组织所搜集的各种.violet[记录]，如人口普查、过往学生的成绩。
- 可以使用旧的、过去的材料（.violet[历史]），如口述、账簿，甚至是别人研究的结论。


--

小样本比较试图结合单案例与多案例分析的优势，同时避免各自的劣势。.orange[一方面]，它保留了每个案例的大量信息（.violet[情境]）。.orange[另一方面]，它比较不同案例从而检验论点（这也是为何单案例分析的说明力很弱，见后文）。通过进行详细的.violet[比较]，它试图回应对单案例分析的典型批判：**单个案例不能得出普遍结论，同时也回应对多案例的典型批判：过度简化，让变量脱离语境从而改变了其意义**。

???
我们必须指出，单案例研究更接近于历史研究，用详细、丰富的数据，以及更重要的是，运用叙事的方式来说明一个案例的典型性、示例性。

---

# 理论问题
## 案例是什么？

这其实是个挺难解释的问题。

在大部分社会科学研究或教学里，当我们用「案例」这个词的时候，我们默认了「.violet[调查对象之间足够相似，又同时是独立个体，由此我们可以把它们视为同一个一般现象的可以比较的实例]（instances）」。

--

「案例之间可比」是所有社会科学的主流认同，同时也是我们其他关于社会生活的话语形式，比方新闻报道、历史故事，所默认的。

- 这个论断包含了某种主观判断
- 没有绝对标准来规定哪些案例是可比的。遵循常规还是推陈出新，研究者需要向她的读者／评审／受众（audience）解释清楚。

---

从社会科学的角度，我们的受众期望研究者对经验证据进行系统评估，以获得社会科学调查的结果。因此，为了让论点立得住脚，我们社会科学家必须依赖证据，**这些证据最好不是孤例，而是全面又广泛的**。

--

所以我们的案例是.orange[典型的]（typical）、.orange[示例性的]（exemplary）、.orange[极端的]（extreme），或者.orange[在理论上具有决定性意义的]（theoretically-decisive）。

--

同时我们还通常假设案例会有**模糊的边界**，案例的形成具有某种开放性和偶然性，认为我们关心的某些属性在特定情境下产生特定意义（例如：试验室之间的位置安排），通过简化复杂的案例进行分析，并允许甚至注重案例间的转变（例如：2023年的澳门大学 vs 2010年的澳门大学）&lt;sup&gt;.red[1]&lt;/sup&gt;。

--

.footnote[
[1] 案例之间，或内部的转变对于历史学而言是一种常态，发生的方式可以是.cyan[出生／死亡]（如一个新研究所的成立）；或.cyan[微观转型]（如同一个名字的机构，其内部人员的更替）、.cyan[蜕变]（如人员构成虽然没变化，但是他们都老了）；或.cyan[宏观转型]（如我们今天称之为教育学院和三十年前在功能上的不同：我们开始关注高等教育研究了）。而要把转变纳入分析，对于大部分量化方法而言非常困难。
]

---

# 理论问题
## 案例不会只有一个

通常，我们在报告或者教学中会引入额外的案例（哪怕我的分析只是针对一个或者某些特定的人），作为.violet[参照]或者.violet[比较对象]，没有参照就很难说明典型性。

--

比方说，我分析过「中国教育追踪调查」（CEPS）的数据。那是针对一个全国7年级和9年级学生的代表性抽样。这批数据可以被看作是对许多案例（.cyan[青年学生]）的广泛研究，也可以被看作对于「中国」的广泛研究。而这里的中国既可以被理解成「.cyan[发展中国家]」的一个案例，也可以被当成是一种理论概念或者理论过程的实例（「.cyan[绩优主义] meritocracy」）。

--

虽然我们很容易将案例研究视为一种.orange[定性分析]的类型，甚至将二者等同起来，但实际上几乎.orange[每一项社会科学研究]，不管是定性还是定量，都可以被视为一个案例研究，因为通常我们可以从多个角度出发去理解它们，因为它是对特定时间和地点的社会现象的分析。

---
# 理论问题
## 设计一个案例

我在这里运用的「案例／个案」是在「实例」（instance）的意义上。实例可以分两种。

--

首先，一个特定的实体可以.red[是某群体的一项实例]。在这里，我们把「案例」作为单元素子集（single-element subset）；群体是一些社会对象（个人、公司）的集合，各案例是其成员。(后文可见，要找到满足这一标准的案例，在统计条件下非常困难)

--

但某一特定实体也可以.red[是一个概念类的一项实例]。在这里，我们将「案例」作为典型（examplar）；概念类具有某种属性（例如它可以是一种结构类型，比方科层制），案例则例示这一属性（exemplify that property）。

???
但是正如没有研究是完美的一样，在许多情况下我们愿意牺牲一些统计上的严格假定，来换取一些别的：

- 第一次研究的现象或群体。
- 罕见的群体。
- 发生在过去的群体。


---

## 案例背后的假设

任何研究方法都有自己的预设立场，案例研究也不例外：

--

我们将案例视为.orange[具有研究者／教师自主定义的复杂属性所组成的模糊现实]（fuzzy realities with autonomously defined complex properties）

--

其次是把案例所代表的现实不再当作一种固定不变的存在，而是.orange[将案例视为与其环境进行永久对话]──一种.violet[行动]（action）和.violet[约束]（constraint）的对话，我们称之为**情节**。

---

## 社会科学研究和教学中的经典案例


- 《幼儿教育与文化》（*Preschool in Three Cultures* by Joseph Tobin）以及其续作[《重访三种文化中的幼儿园》](https://book.douban.com/subject/25913875/)（*Preschool in Three Cultures Revisited*）

- [《一激到底：在竞争环境中抚养孩子》](https://book.douban.com/subject/36187789/)（*Play to Win* by Hilary Friedman）

- [《不谐之音：经济生活中价值的重新诠释》](https://book.douban.com/subject/35814599/)（*The Sense of Dissonance* by David Stark）

---

# 案例研究的重点

## 确定涉及的事件

--

就像人们熟悉的研究法中的「概念」一样，案例事件是假设性的。每位历史学家在决定一桩特定事件是否发生时，都会考虑几十个指示性发生的事（indicating occurrences）。

- 说「十所医学院成立」是一回事，说「医学界对专业教育深切关注」是另一回事
- 描述「十场战役的过程」是一回事，而确定「一场战争的转折」则是另一回事。

--

在分析中，我们会将这些事件安排进一个情节中，把它们设置在我们一般当作是「解释性的松散因果顺序」中。因此，案例总是有某种人为建构的属性在里面。（上文所说的「自主定义」）。


---

# 案例研究的重点

## 刻画事件发生的步骤

以政策分析为例：

- 要找出政策出台的关键步骤，
- 找出谁对每一个步骤都有什么样的能动性（起了什么样的决定性作用）
- 有哪些关键性的决定及其后果
- 谁做的，在哪里做的，如何做的，以及在谁的帮助下做的？
- 然后，政策的出台被看作是一系列重大转折点和由这些事件产生的一系列情境后果。

--

[Suzanne Wilson](https://education.uconn.edu/person/suzanne-wilson/)的*California Dreaming: Reforming Mathematics Education* 是这个领域特别著名的案例研究：美国90年代的数学课程改革为何失败？她以「加利福尼亚州」为例。

---

# 案例研究的重点

## 案例的解释标准

那么什么样的案例分析可以称得上是好的分析？

--

**进入历史人物自己行动的理由的内部**。提出这个观点的是著名历史学家柯林武德（R. G. Collingwood），在《历史的观念》（*The Idea of History*）中，他的立场是当前「理性选择理论」的一个扩大版本；历史学家的任务，是在了解行动者的信念、知识和心理的情况下，去弄清「行动者做什么是合理的」。

--

另一派来自英国哲学家加利（W. B. Gallie）的「**可追随性**」观点。与柯林武德的建构主义一样，这种观点试图描述叙事性历史实际上如何运作。根据这个观点，叙事本身就凭借真理、一致的时间顺序和连贯的中心主体而具有解释力。叙事把由一般规律决定的事物与偶然的事物结合起来，产生一则由于可遵循所以可信的故事。

---

# 案例研究的重点

## 案例叙事的类型

**单案例叙事**：划定案例（delimiting the case），案例可以是事件、群体、事态。难点在于围绕中心主体（central subject）划定边界。比方说，当我以「A研究所为例探讨中国当代科学发展的创新」的时候，如何决定哪些「科学」，何时算「当代」，哪个阶段的研究所呢？

其他的方面我们前文已经提及，然后，我们会从整体开始，解构、简化它：

- 确定涉及的事件
- 把事件按照合理的方式安排进情节里

--

.violet[难点]：第一是情节会交错。一个特定的事件有许多直接的前因，这些前因也各自有许多直接的前因的前因；反之，一个特定的事件有许多结果，这些结果也各自有许多结果的结果。

--

第二，情节有模板，很容易被滥用。比如.cyan[悲剧]（每个人都想讲道理，但还是陷入了困境），.cyan[喜剧]（每个人都很糟糕，但最后事情的结果都是好的），.cyan[浪漫]（黑暗中出现光明），.cyan[讽刺]（事情总是会变得更糟糕，而历史学家的书写无论如何也无济于事）。

--

第三，如何确定情节的起始、中段、和终点？存在终点吗？

---

## 案例叙事的类型

**多案例叙事**，我们试图从多个案例中总结出一些规律

- 最为人们熟知的可能是.orange[阶段理论]（stage theories），我们相信有独特事件的一个共同序列。我们可能会期待一些偏离发生，但一般来说，我们会预期一种自主、稳定的模式（例：发展心理学、发展经济学、家庭生活周期，等等）。
- .orange[生涯理论]（career theories）。生涯理论强调致因的相互作用，结构和决定的混合，必要和充分因果关系间的辩证法（例：教师职业生涯理论&lt;sup&gt;.red[1]&lt;/sup&gt;）。
- 最不确定的（indeterminate）叙事模型是我称之为.orange[互动主义者]（interactionist）的模型。他们具有不确定性，部分原因是他们强调结构；甚至比生涯理论强调地更多，对互动主义者而言，模式化的关系塑造了未来的发展（例：高等教育研究中学科的发展&lt;sup&gt;.red[2]&lt;/sup&gt;）。

.footnote[
[1] https://www.tandfonline.com/doi/abs/10.1080/00228958.2001.10518508

[2]参见拙译《过程社会学》（北京师范大学出版社），第三章中「职业生态和大学生态」部分，其中分析了计算机科学起源于数学和电气工程的例子；以及临床心理学和学术心理学的关系。
]

&lt;!-- 
# 案例研究的重点
## 需要关注的微观过程

案例研究看似简单，但是写出一个令人信服的案例需要考虑微观个体的许多状态：

- 实体.violet[出生和死亡]的过程。对于像生物个体这样的「不可重复」的行动者，这些往往不成问题；对于像「职业」（profession）这样的涌现产物，出生和死亡在概念化和测量上都很困难。在其它重要的实体过程中，还有合并和分裂——同样是容易被贴标签，但不容易被建模的过程。这四个过程都是社会过程叙事中的核心重要事件。

- 另一个基本的实体过程是.violet[微观转型]（microtransformation）——微观层面涌现群体的转型，而其宏观身份或属性的转型可以［不］发生。
  + 微观转型的第一种类型是.violet[更替]（turnover），它可以通过迁移和替换，通过群体中成员生命周期的重组，或者通过新的招募来源，将具有根本不同属性的新个体引入群体。所有这些都可以在群体的自我定义或意识形态等宏观属性不产生任何正式变化的情况下发生。
  + 第二个版本是.violet[内部蜕变]（internal metamorphosis），即成员属性的改变而不发生更替。这可能发生在衰老、集体行动或其它过程中，在这些过程里，仅仅因为成员之间的内部变化（通常是共同变化），群体的涌现属性可能（有时可能不）产生急剧变化。最后，微观转变可能通过微观结构变化发生。一个群体的内部安排可能会发生变化，可能会影响其聚合属性和招募模式。内部层级制度可能导致内部分裂。或者内部重组可能从根本上改变一个群体的行为特征。

- 还有一种实体过程是.violet[宏观转型]（macrotransformation），即涌现实体的根本性变化，而不同时发生微观变化。宏观转型的第一个版本涉及涌现体基本属性产生变化，而没有成分变化。即使成员没有通过更替和其它过程发生变化，职业特性也会迅速变化。在这种情况下，宏观转型可能会改变职业对相关个体的潜在意义。当宏观属性发生变化而.kt[不]影响我们称之为变量的「结构意义」时，就会发生更微妙的宏观转变。职业提供了一则很好的例子。没有一位十九世纪的专业人员会把二十世纪末的任何美国专业人员视为「专业」。现代职业者太过依赖他人，太过频繁地在聚众场合工作，太过频繁地为他人服务，像普通商人一样做广告，等等。然而，二十世纪末的专业人员在今天的劳动力中所占据的.kt[相对]地位与十九世纪的专业人员在当时的劳动力中所占据的相对地位大体相同。专业人员仍然拥有骄傲的地位。这种自豪感的内容不同，但其结构性意义是不变的。这种宏观的转变。我们可以称之为结构同形（structural isomorphism），这在时间研究中很常见。
 --&gt;
---

# 案例有代表性吗？

--

提问题的方式错了。


案例永远不会有「统计代表性」，但这并不意味着案例不能发挥其他作用。

---

# 案例有代表性吗？

## 学生最常见的问题-1：我需要多少个案例？

小王想做一个研究：当代大学生对创新和创业教育（双创）的看法。她学过一些基本的研究设计，计划访谈35位大学生。但是她又担心样本是不是比较小。所以她就问导师：「.cyan[我要怎么做才能保证自己的研究可以有普遍性]（generalizable）？」

--

小王的老师推荐她去一所大学做研究。老师有位朋友在那个学校的学工部，帮她搞到一份学生名册。小王随机从里面选取学生，发手机短信给对方。她知道这种方法，最多只有50%的回复率，于是和导师商量了之后，小王联系了100个人。

--

她运气很好，所有人都回复了她，但是60人拒绝了小王，40人同意接受访谈，35人落实了采访（这已经是非常乐观的数字了）。这所大学的双创教育搞的如火如荼，而那35位学生恰恰好都是报名过「互联网+」大赛的学生。

--

小王进行了35次，每次长达2小时的高质量访谈，深入探讨了这些年轻人对创业的态度。她发现学生的态度和学校双创学院导师的能力之间有联系。她高兴地写完自己的论文，对于其「可推广性」充满信心。

???
家庭里父母辈创业的经验、性别以及祖籍地域

---

### 这是一份标准的研究设计，其问题也是普遍的

从这35人里，小王无论如何也没办法可靠地了解到「当代大学生对双创的态度」。

- 从统计角度，小王只采访到了35%的受访者。这些人有礼貌，愿意抽出时间来和小王交谈。与非受访者相比，这些人对于双创的态度.violet[可能存在系统性差异]。由于她对那些没有作出回应的人一无所知，因此她无法调整从35名受访者那里获得的推论。此外，由于她对于整体大学生就业、创业的情况一无所知，哪怕她能够100%采访到这些人，样本的「典型性」还是不够。
- 不管如何挑选，样本量还是太小了。要回答复杂的问题，需要复杂的样本。什么意思呢？.violet[样本量取决于研究者感兴趣的变量]，这个变量在所有受调查人群里是如何分布的？是纯描述？（点估计）还是需要有计算某种因果关系（「积点较高的学生是不是更不容易投入双创？」）

---

### 统计样本的计算公式

$$
n = \frac{{N \cdot Z^2 \cdot p \cdot (1-p)}}{{(N-1) \cdot E^2 + Z^2 \cdot p \cdot (1-p)}}
$$

其中 `\(Z\)` 是置信区间的临界值（ `\(1.96\)` ）； `\(E\)` 是按照小数点表示的抽样误差（margin of error），比方 `\(0.05\)` ； `\(p\)` 是在总体人群中对双创持有正面、积极态度的比例，来自于先前类似研究的结论，比方 `\(0.51\)`， `\(N\)` 是母体大小。这个公式还有其他许多变体。

参考：http://www.raosoft.com/samplesize.html

--

回到我们的例子，如果小王希望分析大学生里对双创的态度某省有200万大学生（ `\(N\)` ），那么她需要的样本是384人。如果她只关注一个比较小的群体，比方说A大学的1000名大学生，并且她知道这个群体对双创有很支持的态度（ `\(p=0.9\)` ）那么她也需要大约122人。

--

「随机样本」（random sample） `\(\neq\)` 「代表性」（representative）

---

# 案例有代表性吗？

## 常见问题-2：我可以选取一个「普通」的样本吗？（an average case）

许多研究都是这样：如北大路风教授的经典《光变》，

小李想研究一下「高校青椒的生存境遇」，他比小王资深一些，于是他打开了哈佛大学加里·金教授的名作《社会科学中的研究设计》（*Designing Social Inquiry*）

&gt; .kt[研究者可以谨慎地选取一个社区，使它尽量具有代表性，或通过它读者可以理解该社区与其他社区间的关系。……通过询问当地居民或阅读当地报纸可以知道这个社区是否具有代表性……这是案例估计研究工作中最困难的部分，因此要非常谨慎地提防偏差乘虚而入。当确信偏差已经被最小化后，研究者便可以将注意力转到增进有效性上来。要实现有效性的改进，研究者需要花费数周时间总社区中进行大量的研究。]（中文版64-65页）

???
For example, we could first select our community very carefully in order to
make sure that it is especially representative of the rest of the country . . .
We might ask a few residents or look at newspaper reports to see whether
it was an average community or whether some nonsystematic factor had
caused the observation to be atypical . . . This would be the most difficult
part of the case-study estimator, and we would need to be very careful that bias does not creep in. Once we are reasonably confident that bias is minimized, we could focus on increasing efficiency. To do this, we might spend
many weeks in the community conducting numerous separate studies


---
class: clear

### 小李的设计方案

小李翻开一份流传甚广的报告[《青椒：被高校困住的人》](https://mp.weixin.qq.com/s/TNKpt8N5j_N_HguZCXPEpQ)，读了一下青椒的工作状况。

他做了预调研，发现A高校的青椒们普遍很焦虑，不太相信学校的政策，整天想着跳槽。因此他提出了，「对学校的信任是决定青椒留职与否的重要机制」。

小李的做法就是选择一所「典型的大学」。但是问题在于，无论小李如何选择他的一所大学，它都不可能代表所有的大学。

- 但决定大学性质的变量太多了，小李找不到一所在他所选取的特征上处于平均水平的高校。
- 小李依然混淆了「代表性」和「普通／平均」，从统计意义上，「如果使用.violet[抽样单位]进行分析得出的结果与分析.violet[整个群体]时得出的结果相似，那么抽样就被认为具有代表性」。但是我们无法根据样本容量为1的数据达成这个标准。因为这样本没有差异（no variation）。
  + 因此，在统计意义下，单样本案例研究站不住脚&lt;sup&gt;.red[1]&lt;/sup&gt;。

--

.footnote[
.font60[[1] 对这个问题最严厉的批评来自哈佛大学的利伯森，见 Lieberson, Stanley. “Small N’s and big conclusions: an examination of the reasoning in comparative studies based on a small number of cases.” In *What is a case? Exploring the foundations of social inquiry*, edited by Charles Ragin and Howard S. Becker, 105–118. Cambridge University Press, 1992.]
]

---

# 解决方案？

1. 完全忽略这个问题：「我做个案研究，不考虑统计意义上的代表性」

2. 把这项研究理解成「第一步、临时性」，用来「产生新的假设」

3. 从一个不同的角度来重新理解这个研究

---

## 方案-1：完全忽略

.orange[低级版]：师兄师姐同事们都这么做了，我也这么做。

.orange[高级版]：叙事研究的基本世界观本来就和统计研究不同。叙事研究着重的是构建「意义」──行动的意义、选择的意义、等等，而意义只能来自行动／行动者所处的特定环境（the meaning and events can only be explained within the particular set of cirsumstances in which they occurred），意义指向特定的情形（meanings are indexed to particular situations）。

这个回答基本上说，「我们大家生活在平行宇宙里」。

---

## 方案-2：我用案例来生成假设

也就是说，我不是来确认某些统计意义下的特征或者关系的，而是通过案例来生成供后续研究使用的假设。

案例研究是整个「假设-验证／反驳」的实证主义研究序列中的一个环节。

--

逻辑上当然是说得通的，只是在现实里很少有人这么做。

![](https://gdurl.com/s5Z4)

???

alternatively (I didn't get tikz to work)

&lt;!-- {tikz, fig.cap='Exploratory Design 用量化方法检验质性研究所产生的结论'}
\usetikzlibrary{positioning}
\usetikzlibrary{calc}
\usetikzlibrary{shapes.geometric}
\usetikzlibrary{arrows.meta,arrows}

\begin{tikzpicture}[scale=0.95, every node/.style={scale=0.95}]
  % Place nodes
  \node [block] (int) {Interpretation};
    \node [cloud, left = 1 cm of int, align=center] (quan) {quan \\ Data \&amp; \\ Results};
    \node [cloud, left = 3 cm of qual, align=left] (QUAL) {QUAL \\ Data \&amp; Results};
  % Draw edges
    \path [line,dashed] (QUAL) -- node [below]{Build to} (quan);
    \path [line,dashed] (quan) -- (int);
\end{tikzpicture}

 --&gt;
---

## 方案-3：换一种角度看案例

### 3-1 扩展案例法 Extended Case Method

「.violet[一个现象总是和更广的社会力量联系在一起的。这种联系是分析的重点。]」

--

美国社会学家布洛维（Michael Burawoy）认为，在解释性方法中，[案例揭示了整个社会的本质]，就像费孝通先生笔下的江村体现了1930年代中国江南农业（主要是养丝）在资本主义压力下求变的过程。在扩展案例法中，通过研究影响案例诸多条件「更大的力量」来理解案例--开弦弓村的困境反映的是中国农民长久以来面临土地问题，帝国主义入侵和经济关系问题。

--

根据这个思路，「扩展」的是研究者为理解案例所做的工作：研究者调查整个社会，以确定其对当前案例的影响。

---

### 3-1 扩展案例法

研究重点：偏离或独特的案例尤其有趣，因为它们提供了发展或扩展理论的途径。这就为小李的困境提供了一个潜在的合适选择。

已故的英国人类学家克莱德·米切尔（Clyde Mitchell）：

&gt; .kt[扩展案例研究的特殊意义在于，由于它追踪的是案例研究中同一组主要行动者在相对较长时期内参与的事件，因此特别强调［事件的］过程方面。通过扩展案例研究，研究者可以追溯事件之间是如何环环相扣的，因此事件之间又是如何随着时间必然地联系在一起的。](1983: 194)&lt;sup&gt;.red[1]&lt;/sup&gt;

.footnote[
[1]：https://doi.org/10.1111/j.1467-954X.1983.tb00387.x
]

---

回到我们的老问题：「你怎么知道你选择的案例是典型的？」

--

这个问题实则暴露了一种认识上的混淆，即从「统计数据进行推论」与「研究构成案例的元素或事件的特异组合」（Mitchell, 1983: 188）

- .violet[统计推论]：「研究者根据人口中的某些样本，就更广泛群体中存在的两个或多个特征得出结论的过程......」

- .violet[逻辑推论]：「研究者根据某些解释性模式，就两个或多个特征之间的基本联系得出结论的过程」

许多研究者认为，**案例研究只能得出后者，即从任何一个案例研究推断到一般类似情况只能基于逻辑推论。**（参考文献中Mario Luis Small的论文）

---

### 一个例子

假定小李观察到每次国家自然科学基金申报的时候，年轻人很踊跃，因此假设「大学里青椒们对于国自然兴趣更高」，这是错误的。这是一个.violet[描述性的统计推论]，不是逻辑性的（根据上述定义）。而小李的样本不足以让他作出这个推论。

--

但如果小李观察到，每次基金一宣布，青椒们就忙着给导师、委员打电话约喝酒。他就发现了社会网络和投标参与度的关系。而这是一个.violet[逻辑性的假设]。尽管它仍然需要检验，但它在逻辑上是合理的，这使它成为一个有效的假设。

--

前一种假设的形式是「.orange[所有A-类型的实体都会表现出Z-特性]」，这种假设无效，因为根据统计逻辑，这说不通。

后一种假设的形式是「.orange[当 X 发生时，Y 是否会随之而来取决于W是否成立]」。可以根据所观察到的过程来判断其在逻辑上是否合理。

--

两者都是**假设**，都需要进一步检验。但只有后者才是对于案例研究者而言好的假设。

---

class: inverse, middle

### .violet[如果执行得当，单个案例研究可以有理有据地说明，某个特定的过程、现象、机制、倾向、类型、关系、动态或实践是存在的。]

A well-executed single-case study can justifiably state that a particular process, phenomenon, mechanism, tendency, type, relationship, dynamic, or practice exists 

---

### 3-2 顺序访谈 Sequential interviewing

这个解决方案不仅要重新思考她的目标，还要重新思考她对每次访谈的理解。正如我将要展示的那样，这不仅会产生更好的假设，而且会为做出与未观察到的案例相关的经验性陈述奠定更坚实的基础。

This solution involves rethinking not merely her objectives but also her understanding of each of her interviews. As I will show, this will yield not only better hypotheses but also a more grounded foundation to make empirical statements relevant to the cases not observed.

---

### 基础

区分：

.violet[统计抽样逻辑] sampling logic：抽样逻辑是指与标准量化调查研究相关的选择原则。在抽样模式中，要研究的单位（如个人）的数量是.cyan[预先确定的]；样本要.cyan[具有代表性]；所有单位的.cyan[抽取概率应该相等]（或已知）；所有单位必须接受.cyan[完全相同的问卷调查]。如果方法得当，样本的特征有望在一定误差范围内反映总体（population）的特征。目标是**统计代表性**。

--

.violet[案例推论逻辑] case logic：按照顺序进行。这样每个案例都能让研究者越来越准确地理解手头的问题。在案例研究模式中，在研究完成之前，单位的数量（案例）是.cyan[未知的]；从设计上讲，单位的收集.cyan[不具有代表性]；每个单位都有.cyan[自己的选择概率]；不同的单位要.cyan[接受不同的问卷调查]。第一个单位或案例会产生一系列调查结果和问题，为下一个案例提供参考。如果研究进行得当，最后一个研究案例所提供的新信息或令人惊讶的信息将非常少。目标就是**饱和**（saturation）。

--

在提出有关群体的描述性问题时，抽样逻辑更胜一筹；在提出有关研究开始前未知过程的「如何」或「为什么」的问题时，案例研究逻辑可能更为有效。


???

为了阐述和证明案例研究逻辑的丰硕成果，请看一个不是基于访谈而是基于实验的例子。阿方斯做了一个实验：加州理工学院的一组黑人和白人学生被告知他们将接受智商测试，而另一组学生什么也没被告知。两组学生都完成了测试，第一组的黑人成绩比白人差很多，而第二组的黑人和白人成绩一样好。阿方斯的结论是，黑人对智商低的刻板印象的恐惧在起作用。这一推论基于这一个案例。

然而，阿方索意识到，加州理工学院的黑人和白人学生可能与其他地方的学生不同，大学生参加考试的方式也可能与其他人不同。也就是说，他意识到自己并没有统计学家所说的具有代表性的人口样本。此外，他还意识到，严格来说，他的理论并不是关于黑人的，而是关于害怕满足刻板印象对成绩的影响。为此，他又进行了文字和理论上的复制。与杜克大学的一位同事一起，阿尔方斯在杜克大学的本科生中重复了这个实验（文字复制）；回到加州理工学院后，他又重复了这个实验，但使用的是女性和男性，而不是黑人和白人，因为他知道存在着女性无法在数学测试中取得好成绩的刻板印象（理论复制）。阿方斯推断，如果理论是正确的，那么它应该适用于任何人，而不仅仅是黑人和白人。

一些测试证实了他的发现，另一些则没有。然后，他又在亚洲人和白人之间，在智商以外的其他问题上，在更多的校园里，在高中生和老年人之间，不断地进行试验。每做一次实验，他都会完善和重新评估自己对实验过程的理解。慢慢地，随着实验（即案例）数量的增加，他对自己理论正确性的信心也开始增强。最终，每一次新的实验都只会带来很少的新知识，例如，第 89 次实验是在一所低收入高中进行的，实验对象是亚洲移民和俄罗斯人，实验结果与他预期的完全一致，尽管他预期的关系要比他在第一次实验中设想的微妙得多。至此，他已经达到了饱和状态。

---

class: clear

## 重新回到小王和小李的例子里

关键是要把每一个人都看成一个单独的案例。

小王在不知道最终会采访多少个受访者的情况下，先访谈一个人。经过两个小时，举个例子，这个人回忆并讲述了她小时候看到自己亲戚开店成功的经历，因此对创业产生了乐观情绪。

通过访谈，小王开始对她最初的问题──大学生对双创教育的态度──有了新的认识。她推断，对于创新和创业，有结构性的因素，她分开两路，一边去找哪些有过去创业成功经历（或目睹）的学生，以及那些完全的创业新手。

重要的是，她在每次新的访谈中都会对双创的不同方面提出越来越细化的问题，因为每次访谈都在不断完善并迫使她重新评估自己对这一现象的理解。她多次重复这一过程。她的下一次访谈比上一次访谈时间更长，访谈的内容也更细腻，包括许多她在第一次访谈中没有想到的创新体验。最终，每次新的访谈都会告诉她一些她以前没有听说过的关于创新和过去经验之间关系的信息。她已经达到了饱和状态。

小王的新方法几乎违反了（频率主义）抽样逻辑的所有原则。她的受访者群体不具有代表性；每个受访者收到的问卷略有不同；没有试图尽量减少统计偏差。因此，小王无法对态度的分布做出准确的陈述。例如，她不会报告说 25% 的大学生赞成双创教育。然而，我们对她的实证研究结果是有信心的。

---

# 案例作为教学方法

## 案例教学为何能起作用？

要回答这个问题，让我借助牛津大学教授.violet[弗吕夫布耶格]（Bent Flyvbjerg）的经典论述&lt;sup&gt;.red[1]&lt;/sup&gt;：

1. 当我们试图从「依照规则行事的初学者」（rule-based beginners）转变到自然行事的专家（virtuosos experts）阶段时，案例研究所产生的知识是一个必要条件。

2. 在对人类事务的研究中，大量的知识是「依赖情境的知识」（context-dependent knowledge），而非抽象存在的。

.footnote[
[1] https://doi.org/10.1177/1077800405284363
]
---

class: clear

### 哲学家维特根斯坦如是说

.pull-left[
"In teaching you philosophy I’m like a guide showing you how to find your way round London. I have to take you through the city from north to south, from east to west, from Euston to the embankment and from Piccadilly to the Marble Arch. After I have taken you many journeys through the city, in all sorts of directions, we shall have passed through any given street a number of times — each time traversing the street as part of a different journey. At the end of this you will know London; you will be able to find your way about like a born Londoner. Of course, a good guide will take you through the more important streets more often than he takes you down side streets; a bad guide will do the opposite. In philosophy I’m a rather bad guide."

]

.pull-right[
.kt[「在教你哲学时，我就像一位向导，带你了解伦敦的方方面面。我必须带你从北到南，从东到西，从尤斯顿到泰晤士河岸，从皮卡迪利到大理石拱门。在我带你通过城市的各种方向多次旅行后，我们将会数次经过所有的街道——每次都作为不同旅程的一部分穿越这些街道。在此之后，你将了解伦敦；你将能像土生土长的伦敦人一样找到你的路。当然，好的导游会经常带你穿过更重要的街道，而不是小巷；糟糕的导游会做相反的事情。在哲学中，我是一名相当糟糕的向导。」]

*Source*: Gasking, D. A. T., &amp; Jackson, A. C. (1967). Wittgenstein as a teacher. In K. T. Fann (Ed.), *Ludwig Wittgenstein: The man and his philosophy (pp. 49-55). Sussex, UK: Harvester Press.*
]

---
# 用案例来教学

### 我们首先要理解人类如何「学习」

对于成年人的学习过程而言，存在着一个质的飞跃（a qualitative leap），即从初学者对分析理性的规则性运用（from the rule-governed use of analytical rationality）到技艺高超的熟练运用（*virtuosos*）

这些技艺高超的人在各行各业里广泛存在：下棋、弹琴、课堂教学，等等。

--

他们的共同特征？精湛的技艺建立在数百上千次在具体情境中的实践知识基础之上（on the basis of intimate knowledge of several thousand concrete cases in their areas of expertise）。

依赖情境的知识和经验是必要条件，仅仅有分析理性是不够的。

--

此类知识和经验还有一个特征：.orange[只可意会，不可言传]。学界给了此类知识不少术语：默会知识（tacit knowledge），或情景学习（situated learning）。

---

class: clear, middle

.pull-left[
### 因此，案例法被许多知名学府，尤其是他们的职业学院（professional school）用于教学。其中最知名的就是哈佛大学（Harvard）的商学院、教育学院、医学院：

]

.pull-right[
![](./asset/christensen-case.png)
]

---

# 案例教学的核心

好的案例 + 耐心的解读

![:wscale 68%](./asset/case-teaching.png)

---

## 利用案例的诸多不同进路

北京大学.violet[陈向明]教授：教育叙事行动研究
- .kt[「我们之所以选择叙事探究方法来开展行动研究，是因为这种方法特别适合一线教师了解和改进自己的工作。教师工作具有情境性、关系性、过程性、复杂性和价值性等特征，而讲故事的方式能够让这些特征浮现出来，随后的行动干预能够有助于改进教育现状。与叙事话语相比，科学话语过于宏大，操作程序过于严苛，不仅难以被教师掌握，而且很难解决他们工作中遇到的真实难题。」][出处](https://mp.weixin.qq.com/s/0kM8P9LH31k5dhmBQreNxg)

--

东京大学.violet[佐藤学]教授：学习共同体
- .kt[「所謂學習，是同課題（教材）的對話；是同他人（夥伴與教師）的對話；也是同自己的對話。我們通過同他人的合作，同多樣的思想碰撞，實現同課題的新的對話，從而產生並雕琢自己的思想。…… 學習是從已知世界出發，探索未知世界之旅；是超越既有經驗與能力，形成新的經驗與能力的一種挑戰。」] （《學習共同體：構想與實踐》，p.20）

---

## 利用案例的诸多不同进路

剑桥大学.violet[亚历山大]教授（Robin Alexander）：对话式教学（dialogic teaching）

- collective .orange[集体性]：教师和孩子们一起处理学习任务，无论是作为一个小组还是作为一个班级，而不是孤立地学习；
- reciprocal .orange[互惠性]：教师和孩子们相互倾听，分享观点，并考虑替代的观点；
- supportive .orange[支持性]：孩子们能自由地表达自己的想法，不用因为“错误”的答案而感到尴尬；他们互相帮助，以达到共同的理解；
- cummulative .orange[累积性]：教师和孩子们基于自己和他人的想法，将它们串联成连贯的思考和探究线路；
- purposeful .orange[目的性]：教师们以特定的教育目标为视野，计划和促进对话式的教学。

.footnote[
目前尚无中译，原文参见：Alexander, R. (2020). *A Dialogic Teaching Companion* . Abingdon, Oxon: Routledge. 
]
---

# 案例教学的起源

从教师的角度出发，专业实践（professional practice）经历了两个维度的转变：

--

- 从.orange[科技理性]（Technical rationality）转向.orange[行动中的省查]（Reflection in action）

--

- 从.orange[技术熟练者]（Technical expert）转向.orange[反思性实践者]（Refelective practitioner）

--

.footnote[反思性实践者的观念见：Donald Schon, _The Reflective Practitioner: How Professionals Think in Action_, 1983；中文版参见[《培养反映的实践者》](https://book.douban.com/subject/3350233/)]

---

# 那么案例教学的理论基础是什么呢？

目前诸多主流的案例教学流派，大都受到.violet[杜威]（John Dewey）教育思想的影响。

在杜威那里，案例的意义在于给.orange[行动]（action），.orange[经验]（experience），.orange[探究]（inquiry）提供了一个交汇点。

--

- 人类行动是人作为元素和环境之间互动的一种模式。有意向的动作（intentional behavior）才是行动。只有通过行动，「现实」在会显示出来──某个问题的解决方案、某种答案的有效性、某个思路的过程。

--

- 学习发生在特定环境条件下，个体的需要、欲求、目的和能力与这些条件的互动，积淀下来形成了经验。「.kt[在『做』与『经受或遭受』之间的紧密联结形成了我们所说的经验。]」《哲学的改造》。

--

- 有些经验是没有目的的偶然所得，称之为「经历性经验」（experience as empirical）；但有些经验来自于人类通过操作，系统探究「行动条件和行动结果」之间的关系而获得经验，称之为「实验性经验」（experience as experimental）。

--

- 在实验性经验里，占据主导地位的是「思考」或「反思」。

---

## 实验性经验怎么来？

来自学生在老师引导下的探究

&gt;.kt[探究是控制性地和引导性地把一个不确定的情境]（indeterminate situation）.kt[转变为一个确定性情境]（determinate situation）.kt[的过程。这种确定性体现在能确定地区分新情境的构成部分，并能够确定地知道使得原初情境中的各个要素转变为一个统一协调体（情境）的各种关系]《逻辑：探究的理论》

--

这个探究的过程，也就是我们所说的「.red[致知／识知]」（knowing）的过程，.violet[它是探寻行动及其后果之间的关联，并重新组合行动的过程]。

--

案例教学的实质，是通过案例，还原一个受控且重要的情境。案例的成功与否，取决于老师能不能将学生在现实中碰到的问题（上文所说的不确定性），放在一个具体材料的情况下，通过观察、讨论，结合已有的知识，提出解决问题的建议，最终导向可能的行动方案。

---

# 案例教学的目的

并不是一劳永逸的找到一种特定的解决方案，一种特定的知识。而是培养解决和分析问题的能力，杜威称之为「明智」（intelligence）：

--

&gt;.kt[一个人之所以是明智的，并不是因为他有理性，可以掌握一些关于固定原理的根本不可证明的真理，并根据这些真理演绎出其所控制的特殊事物，而是因为他.orange[能够估计情境的可能性]，.orange[并能根据这种估计来采取行动]。在广义上，明智的特征是关于实践的，正如理性的特征是关于理论的一样。只要明智运行，某些事物就以它们能够预示其他事物的迹象而被判断着。]《确定性的寻求》


---
# 文献

Anderson, E., &amp; Schiano, B. (2014). *Teaching with cases: A practical guide*. Harvard Business Press.

Barnes, L. B., Christensen, C. R., &amp; Hansen, A. J. (1994). *Teaching and the case method: Text, cases, and readings*. Harvard Business Press.

赵康（2021），《.kt[教师的实践性知识和实践性判断]》，教育科学出版社。

Flyvbjerg, B. (2006). Five Misunderstandings About Case-Study Research. *Qualitative Inquiry*, *12*(2), 219–245.

Small, M. L. (2009). 'How many cases do I need?' On science and the logic of case selection in field-based research. *Ethnography*, *10*(1), 5-38.

「案例做什么？」出自安德鲁·阿伯特［著］，周忆粟［译]，《.kt[攸关时间：理论与方法]》，北京师范大学出版社（预计2024.6）。
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="./libs/plugin/wscale.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
