---
title: "Methods of Discovery"
author: "Yisu Zhou"
date: "17/08/2019"
output:
  html_document: default
  pdf_document:
    keep_tex: yes
  word_document: default
---

# Pt 1. The Basics

Reading: Abbott, Andrew. "Methods of discovery: Heuristics for the Social Sciences." (2004).

- Chapter 1 MoD: Explanation
- Chapter 2 MoD: Basic debates and methodological practices


## What does it take to have something to say?

1. A __puzzle__: something about the social world that is odd, unusual, unexpected, or novel.
2. A __clever idea__ that responds to or interprets or solves that puzzle. 

## Difference between disciplines

No single criterion for the distinction among disciplines

- __Economics__ is organized by a theoretical concept (choice under constraint)
- __Political science__ by an aspect of social organization (power)
- __Anthropology__ by a method (ethnography)
- __History__ by an aspect of temporality (the past)
- __Sociology__ by a list of subject matters (inequality, the city, the family, etc)

## Explanation

Explanation is the purpose of social science. What makes an explanation?

1. **Pragmatic**: _We say something is an explanation when it allows us to intervene in whatever it is we are explaining_. We have explained poverty when we know how to eradicate it. Works best for phenomena that have somewhere a narrow neck of necessary causality (p.9)
	+ Example: Germ theory of health

2. **Semantic** (reductionism): we say an account explains something when we stop looking for further accounts of that something. _An explanation is an account that suffices._ i.e. explain all human activities without any reference to group phenomena (p.8). It defines explanation as _translating_ a phenomenon form one sphere of analysis to another until a final realm is reached with which we are intuitively satisfied. (p.10) 
	+ Example: utilitarians "explain" personal behavior as an outcome of selfishness, because the latter is more real, intuitive, than any other.

3. **Syntactic**: We have an explanation of something when we have made a certain kind of _argument_ about it: an argument that is simple, exclusive, logically beautiful and compelling, perhaps elegant or even counterintuitive. In this sense, an account is an explanation because it takes a certain pleasing form, because it somehow marries simplicity and complexity. _covering law_ (Hempel)
	+ Example: Piaget's theory of intelligence. He argues that reality involves _transformations_ and _states_. He postulated if human intelligence is adaptive, it must be able to capture these two aspects. He proposed that operative intelligence is responsible for the representation and manipulation of the dynamic or transformational aspects of reality, and that figurative intelligence is responsible for the representation of the static aspects of reality.
	+ Example: Golden Arches Theory of Conflict Prevention

## Methods

One might expect that various social science methods would be version of a single explanatory enterprise or that they would be logical parts of some general scheme, but in practice they don't work that way. Far from being parts of a general scheme, they are somewhat separated from one another and often __mutually hostile__ (see "debates" below). 

There are multiple ways to classify methods:

- **Categorize by types of data gathering**

	1. *ethnography:* gathering data by personal interaction
	2. *surveys*: gathering data by submitting questionnaires to respondents or formally interviewing them
	3. *record-based analysis*: gathering data from formal organizational records (census, accounts, publications, etc)
	4. *history*: using old records, surveys, and even ethnographic

- **Categorize by how we analyze data**

	1. *direct interpretation*: analysis by an individual's reflection and synthesis (for example, narration)
	2. *quantitative analysis*: analysis using one of the standard methods of statistics to reason about causes
	3. *formal modeling*: analysis by creating a formal system mimicking the world and then using it to simulate reality

- **Categorize by how one poses a question**

	1. *case-study analysis*: studying a unique example in great detail
	2. *small-N analysis*: seeking similarities and contrasts in a small number of cases
	3. *large-N analysis*: emphasizing generalizability by studying large numbers of cases, usually randomly selected.

Therefore there are  $4*3*3=36$ possible subtypes. Successful methodological traditions:

- Ethnography: living inside the social situation one is studying and becoming to some extent a participant in it. _concrete_ version of _semantic_ program, $1*1*(1/2)$
- Historical narration, _concrete_ version of the _syntactic_ program, $(3/4)*1*(1/2)$
- Standard causal analysis, _abstract_ version of the _progmatic_ program, $(2/3)*2*3$
- Small-N comparison $(1/4)*1*(1/2)$, usually comparison
- Formalization, _abstract_ version of the _syntactic_ program $?*3*(1/2/3)$

## Examples

1. Andrew Kipnis: http://bit.ly/2vZyN7a
2. Cong Xiao Ping: https://book.douban.com/subject/2444005/
3. Hong Guang Lei: http://bit.ly/2LK5PD1
4. Kathleen Thelen, Vocational education: http://bit.ly/2Vln9xH
5. Dennis Epple and Richard E. Romano, Cream the top: http://bit.ly/2WRXbTK 

Other examples:

- Mixing things up: 
	+ _Challenger Launch Decision_ (Diane Vaughan https://book.douban.com/subject/2856654/) 
	+ _Friendly Fire_ (Scott Snook https://book.douban.com/subject/2946005/)
	+ _Death Foretold_ (Nicolas Christakis https://book.douban.com/subject/2418036/)


## Chapter 2. Basic debates

Abbott believes that methodological debates are based on philosophical differences: 

>"I will show that on closer inspection, the usual, simple picture of the methods comes apart in our hands. In the first place, *each* method offers a profound critique of *each* of the others, critiques that are aligned along quite different dimensions. As a result, the various methodological critiques can be arranged in tail-chasing circles." But this is not necessarily a bad thing: "This circular quality guarantees an openness, a heuristic richness, to mutual methodological critiques." (p.42)

Basics (p.52, Table 2.1):

- Metholodogical debate
	+ _Positivism_ vs. _Interpretivism_
	+ _Analysis_ vs. _Narration_
- Ontological debate
	+ _Behaviorism_ vs. _Culturalism_
	+ _Individualism_ vs. _Emergentism_
	+ _Realism_ vs. _Constructionism_
	+ _Contextualism_ vs. _Noncontextualism_
- Problematics
	+ _Choice_ vs. _Constraint_
	+ _Conflict_ vs. _Consensus_
- Types of knowledge
	+ _Transcendent knowledge_ vs. _Situated knowledge_


### Methods & Debate

This section views the above-mentioned debate through the lens of 5 methods
- P.54 Table 2.2
- Cycles of critique
- From critiques to heuristics

![](./pic/table2.2.png)

### Fractals

![](./pic/fractal.gif)


### Example: The Bella Coola study (p. 62-64)

- BC is a native American tribe in western Canada. 
- Anthropolosit Levi-Strauss studied their myth (using _ethnography_, i.e. observation)
	+ He made a connection between mythic structure and the clan structure.
	+ Claims "the meth system is in fact a loose cultural picture of the clans". 
	+ The clans use the myth system to talk about, modify, undercut, and manipulate the social structure of everday life
- But BC is just one tribe. The Human Relations Area Files collected similar data on hundreds of other societies and develop a classification & coding scheme for the myth system. So one can do _SCA_ with these quantitative data (i.e. more general analysis)
	+ Normal science: more data, more variable, newer quant models
- _Historian_s came in to historicizing these categories and coding schemes:
	+ the myths and physical anifacts were produced for, and therefore determined by, the demands of anthropologists, museum workers, and other collectors of "primitive material."
	+ These mythes are produced for an "anthropology trade"
	+ The social structure is changed after contact with modern society
- It means the _SCA_ is completely invalid
- But one can equally study such culture contact. Typically through an _ethnographic_ approach. Maybe the process of culture contact as a repeated-play Chiken game:
	+ Each time the contact recurs, both sides attemp to enforce one of the other transforms its interpretation through a complete redifinition, which only last until the next play.
	+ Or they can view it as under a power dynamic
	+ But either way, these new perspective opens up new possibility for doing something different. 

# Pt 2. Design A Project

Reading

- Chapter 3 of MoD, Intro to Heuristics
- Chapter 6 of MoD, Fractal Heuristics
- Chapter 7 of MoD, Ideas and Puzzles

## Heuristics

Origins in mathematics (is Math a science? It is debatable.)

But social scientists do it *backward*: Their original research proposals usually turn out to have just been hunting licenses, most often licenses to hunt animals very different from the ones that have ended up in the thesis/dissertation.

Most teaching of methods:

![](./pic/focus.png)

But:

- Most research projects start out as general interests in an area tied up with hazy notions about some possible data, a preference for this or that kind of method, and as often as not a preference for certain kinds of results. 
- Most research projects advance on all of these fronts at once, the data getting better as the question gets more focused, the methods more firmly decided, and the results more precise.
- At some point - the dissertation-proposal hearing or grant-proposal stage for faculty, the office hour with the supervising faculty member for any serious undergraduate paper -- an attempt is made to develop a soup-to-nuts account of the research in the traditional order. 
- Now emerges the familiar format of puzzle leading to literature review leading to formal question, data, and methods.

In what ways does Heuristics help?

- What can be said?
- What has been said? 

Transform the past into new ideas and new views. On the other hand, steady practice of heuristics will teach you rules for separating good things that could be said from bad ones. Typical process:

- Stage 1: Have nothing to say
- Stage 2: Ask stock questions (normal science, Kuhn) 
	+ more data 
	+ new dimension
	+ adding new model or methodological wrinkle or theoretical twist
- Stage 3: Intellectual development
- Stage 4: Discussion of mutual criticism.
	+ An example: https://doi.org/10.1177/0735275114523419


## Chapter 3. Into the heuristics

Note: no research attack all these things AT ONCE. People will make convenient assumptions one way or another.

### Aristotle's Four Causes

- Material cause 质料因：共和党输给了民主党，因为他们输掉了女性选民。弱势学生成绩不好，因为缺乏家长投入。
- Formal or structural cause 形式因：三个和尚没水喝。社会收入差距的增大导致学生成绩之间差异的增大。
- Effective cause 动力因：工人上街导致企业主报复。
- Final cause 目的因：大学出现的原因是因为需要教育。制定环保法的原因是因为我们需要清洁空气。

Every event has causes of all 4 kinds:

- 环保法，是为了空气质量（final），也可能是利益集团游说（effective）。具体的条款可能是由立法机关里的力量对比造成的（structural）
- 教师毕业找不到工作：
	+ material：什么样的人成为老师（年轻女性）
	+ effective：谁做出雇佣的决定？谁决定解雇？有没有什么原因（如继续教育的成本很低）导致人们原意不找工作？
	+ structural：供过于求？
	+ functinoal：工作给人提供的也许不单单是金钱？有人原意底薪从事原本的工作。

### Kant's four categories

康德在《纯粹理性批判》里对知性的逻辑功能与范畴所做的四种区分

- Quantity 量 (unity, plurality, totality)：who is? how would people count? how unified?
- Quality 质 (reality, negation, limitation): reification 社会化（functional argument）
- Relation 关系 (substance/accidents, causality/dependence, reciprocity)：思想政治老师是干什么的？；循环促进关系（收入和职业）
- Modality 模态  (possibility/impossibility, existence/nonexistence, necessity/contingency): 每个人都可能成功吗？社会的收入可能人人一样吗？什么样的情况「社群」成为群体？

### Kenneth Burke's Five keys of dramatism 

*A Grammar of Motives*: **action**, **actor**, **agent**, **setting**, **purpose**.

### Charlse Morris's Three aspects of symbolic system

- __Syntactic__ relations: relations between elements of the system.
- __Semantic__ relations: are relations between system elements and things to which they refer.
- __Pragmatic__ relations are relations between symbolic statements and the context of action in which they are made.

### Other lists

- Disciplines as commonplace list: What will economists think? What would an anthropologist say?
- Social functions: Talcott Parsons's adaptation, goal attainment, integration, and pattern maintenance. 

### Words of caution

1. Do not reify these lists. They are simply useful lists of reminders of things to think about, reminders to use when you get stuck. Don't worry about their reality or truth.
2. Don't overuse them. Don't use these lists as some kind of comprehensive system that you put each of your research questions through. Use them to stimulate your thinking. 


## Chapter 6. Fractal heuristics

How fractal social science debate opend up new questions and possibilities

- Sociology of science example (p. 163, within sociologists of science, structure vs. daily interaction)
- Crime (reslist stats vs constructionist labels)
- Anxiety and Stress

The main importance of the fractal debates may not be as organizing principals of the disciplines, but rather as heuristics for the disciplines. 

Pick one of the 9 contrasts and explain to your peers. 

## Chapter 7. Ideas and Puzzle 

### Tests of Idea

- We test ideas on ourselves
	+ Looking for other cases of a phenomenon or a relationship you have identified
	+ Looking for other implications that you idea has for data.
- Mutual test (get others to test your ideas)
	+ simple summary
	+ taxi driver test
- Finally, we test ideas with respect to existing scholarly writing on a topic.
	+ literatures work by making simplifying assumptions about some things so that researchers do complex analyses of other things. 

Get into the habbit of continually generating implications and continually moving your ideas on to new cases or data.

### Puzzles

- Knowledge
- Taste
- External puzzle generator
- Identity research (personal motivation)
- Truly disinterested curiosity

### Wrapping up

MoD worth in-depth reading. Chapter 4-6 offers detailed example how heuristics/mutual critique is used as a generative force to produce new ideas.

A mindmap of MoD: https://coggle.it/diagram/Wynm2Y-62wXoP7_v/t/methods-of-discovery-heuristics-sciences-by-andrew-abbott