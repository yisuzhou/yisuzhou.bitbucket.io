# Install packages. Skip this step if you already have them
install.packages("haven") # read data
install.packages("dplyr") # data wrangling
install.packages("tidyverse") # plotting
install.packages("xtable") # format tables
install.packages("flextable") # export table to Word

# Load packages

library("haven") 
library("dplyr") 
library("tidyverse")
library("xtable")
library("flextable")

# If you know where your data file is, run:
sample<-read_spss("sample.sav")

# Otherwise. Pick one.

sample<-read_spss(file.choose())

# subsetting data
hk<-subset(sample,CNT == "HKG")
hkmo<-subset(sample, CNT %in% c("HKG","MAC"))

# frequence table of gender in HK
table(hk$ST004D01T)

# summary statistics

summary(hk$ESCS)

# summarize 1st plausible value on math score in HK by gender

hk%>%group_by(ST004D01T)%>%summarise(
									N = length(PV1MATH),
							mean_math = mean(PV1MATH),
							sd_math = sd(PV1MATH)
							)

# Correlation matrix, omitting missing data
# Five variables: SES, ICT resources, math score (1st pv), science score (1st pv)
hk%>%select(ESCS,ICTRES,PV1MATH,PV1READ,PV1SCIE)%>%na.omit()%>%cor()

# histogram (requires tidyverse)

ggplot(data=hk, aes(x=ESCS)) + geom_histogram(bins = 20)

# can also save the graph to a file

ggsave(file="histogram.png")

# scatter plot

ggplot(hk, aes(x =ESCS, y = PV1MATH)) + geom_point() 

# fit a linear regression model

mod <- lm(PV1MATH ~ ESCS, data=hk, weights=W_FSTUWT)

# view the result

summary(mod)

# export tables to a Word file
mod%>%xtable()%>%xtable_to_flextable()%>%save_as_docx(path = "example.docx")
