<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Education &amp; Globalization</title>
    <meta charset="utf-8" />
    <meta name="author" content="Dr. Zhou Yisu 周憶粟" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-fonts.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-extra.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: top, left, inverse, title-slide

.title[
# Education &amp; Globalization
]
.subtitle[
## Session 19: Shadows of globalized education
]
.author[
### Dr. Zhou Yisu 周憶粟
]
.date[
### 2023/10/26
]

---





# Shadows of education
## Aims

To examine the negative global flows of education

To consider the impact of these flows on individuals

To understand some theoretical explanation of the negative flow

---

### Apart from last session's discussion, we have more evidence of discontent

.pull-left[
### Being A Child: Singapore
![](./asset/maxresdefault.jpg)
]

.pull-right[
### Korea's mounting academic pressure
![](./asset/maxresdefault2.jpg)

]

---

## Common theme among East Asian school systems

Both Singapore and Korea engaged in .violet[competitive schooling]

- .cyan[getting into a "good" school] in Singapore is extremely important because parents believe any advantage early on would help their children in later examinations

  + Singapore is one of the few countries track/stream children systematically 分班、分流

--

- Of course, just studying in school is not enough. Many countries like Korea have an elaborate after school tutoring (.cyan[shadow education]) industry.
  + remedial purpose: to make up for what schools don't teach
  + enrichment purpose: to develop expertise

--

Common consequence: .violet[Stressed-out students &amp; family]
- Psychologically
- Socially

---

## School system: Macao
![:wscale 80%](./asset/flow-macau.png)

---

## School system: Singapore
![:wscale 80%](./asset/flow-singapore.png)


.footnote[Note: Singapore has recently reformed its school system: https://www.tutopiya.com/blog/singapore-school-system-the-stages-of-education/]

---

## School system: South Korea
![](./asset/flow-korea.png)


---

## Nature of competitive schooling
![](./asset/shadow1.png)


---

## The "Shadow" metaphor
Shadow education is essentially more education. But there are several key characteristics:


.orange[Supplementation] 補充: more education on top of common school experience


.orange[Privateness] 私有: tutoring provided by private entrepreneurs and individuals for profit-making purposes.


.orange[Academic] 學術性質: language subjects, mathematics, and other examinable subjects. 

---

## Shadow is almost everywhere

In .violet[Hong Kong], a 2011/12 survey found that 53.8% of G9 students and 71.8% of G12 students were receiving private supplementary tutoring &lt;sup&gt;.red[1]&lt;/sup&gt;.

--

In .violet[India], a 2012 survey indicated that 73% of children aged 6-14 were receiving tutoring.

--

In .violet[Kazakhstan], a 2005/06 survey of university students found 59.9% had receive tutoring during their last year of secondary school.

--

In .violet[Vietnam], 2006 survey indicated 32% of primary students, 46% of lower secondary, and 63% of upper secondary students received tutoring.

.footnote[[1] Source: https://cerc.edu.hku.hk/wp-content/uploads/Mono-10.pdf.

More recent data: https://doi.org/10.1177%2F2096531119890142 or https://cerc.edu.hku.hk/wp-content/uploads/M14-Optimized.pdf]


???
Researchers started to collect international data on this topic about a decade ago.

---

## Shadow is almost everywhere

More detailed data from .violet[Singapore]:
- 60% of high school, and 80% of primary school age students receive private tuition.


- 40% of pre-schoolers receive private tuition.


- Pre-schoolers, on average, attend two hours private tuition per week, while primary school aged children are attending, on average, at least three hours per week.


- According to Singapore's Household Expenditure Survey, private tuition in Singapore is a SGD 1.1 billion dollar industry almost double the amount households spent in 2005 (SGD 650 million).

---

## The shadow spawns a big business

.pull-left[
![](./asset/shadow2.png)
]

.pull-right[
![](./asset/shadow3.png)
]

???
In China, it is estimated to be a 10 billion business, though wiped out overnight in 2021.

---

## The shadow spawns a big business

.violet[uneveness] of the globalized age

![](./asset/shadow3.jpg)

.footnote[Source: _Fast earners: South Korea's millionaire, celebrity schoolteachers_ https://www.theguardian.com/world/2015/jan/16/south-korea-teachers-exams-education]

---

## Negative consequences: student suicide

![:wscale 75%](./asset/singapore3.jpg)

.footnote[Source: https://www.zaobao.com/znews/singapore/story20161022-680662]

---

## Negative consequences: Mental illness

When children are being asked to achieve perfection in every level, their minds some times could not adapt very well &lt;sup&gt;.red[2]&lt;/sup&gt;

--

- When parents are highly intrusive 高度介入 and controlling, children are more likely to be .violet[self-critical] 自我批評, .violet[anxious] 焦慮, and .violet[depression] 憂鬱.

- Intrusiveness also indicates to children that they are not good enough and that parents have to intervene in various domains of their lives. 

- As a consequence, these children might become .violet[overly concerned] about committing even the slightest errors.


.footnote[[2] Hong et al., 2016, _Developmental Trajectories of Maladaptive Perfectionism in Middle Childhood._ http://onlinelibrary.wiley.com/doi/10.1111/jopy.12249/abstract]

---

background-image: url(./asset/attitude2012.png)
background-size: contain

&lt;!-- attitude toward school, pisa 2012 --&gt;

---

## Broad implications

Impact on .cyan[citizenship]: societies divided along socioeconomic line (those who can and cannot afford more education at private expense)

--

Education becomes a .violet[commodity] 商品:

- Goods for sale

- Students/parents are consumers and schools are service providers

- Consumers demand differentiating learning experiences

--

Created a "losing culture": Only tiny fraction of successful winners but a large army of failed ones.

--

Skew motivation for learning

---

.pull-left[
![](./asset/p2572640071.png)
]

.pull-right[
### .violet[Anxiety marketing:]

### "We cultivate your child if you come; We cultivate his competitors if you don't."
]

---

class: inverse, middle, center
# Why do people want _more_ education?

---

## If more education is so bad, why do people still want it?

--

Scholar Clark Sorensen pointed out: &lt;sup&gt;.red[3]&lt;/sup&gt;

- Simple answer: **It is not you who want more education. It is your family.** And it is not just because they have a preference for specific subjects.

--

- Unique parent-child relationship in Asia: parents expected to be cared for by their children in old age and .violet[filial piety] 孝道 by giving them a good funeral, worship them after death, is core of Korean ethics (as well as many other societies).

--

- .violet[Reciprocal dependence] 相互依賴: parents invest in children and expect payback in old age

- Family's social status depends on children's academic/economic success

.footnote[[3] Sorensen, C. W. (1994, February). Success and education in South Korea. _Comparative Education Review, 38_(1), 10-35. Retrieved from http://www.jstor.org/stable/1189287]

???

This co-dependence is an important factor. Other than the parents, there are very limited pathway where a young people can support him/herself into an independent life.

---

## The cultural explanation 文化解釋

This explanation originates from the very unique (Asian) family culture:

- Parents are more likely to .violet[interfere] 干預 with children's learning and social choices rather than respect children's own will.

--

- Parents have the time, and inclination, and the power to .violet[demand] that their children succeed in school. 

--

- Inside schools, teachers request .violet[respect] 尊敬 and .violet[compliance] 順從 from students while convincing them that they care about them as human beings and have their best interests at heart.

--

- .orange[It is the effort, not ability that matters to the parents and family]  (and that constitute a cultural system).

---
class: middle, center

## However, culture alone couldn't explain the zeal people have on education

--

## It requires a powerful structural ally: the idea of .red[social mobility for the individual]

---

### A common theme in Western or Eastern TV dramas: succession

.pull-left[
![](./asset/got.jpg)
]

.pull-right[
![](https://i2.letvimg.com/lc04_isvrs/202211/16/16/04/5040dd2a-8222-4959-ad9f-b72e81fd97bf.jpg)
]

&lt;!-- Just a generic "甄環傳" --&gt;

???

Traditional or modern; fictional or historical

---

### Why is Jon Snow the King of the North? Not Lord Baelish (aka Little Finger)?

![](./asset/qcVdXpC.jpg)

---

## In the Game of Thrones world (and other ancient worlds), .cyan[last name] matters a lot

![](./asset/6901923-6424922-jon+and+ned.jpg)

---

## Social mobility in feudal societies

.pull-left[
![](./asset/1398505558.jpg)
]

.pull-right[
![](https://cdn.storyboardthat.com/storyboard-srcsets/liane/ancient-china-social-structure.webp)
]

&lt;!-- Just a generic "social class in ancient china" --&gt;

???
There are very limited social mobility in the ancient societies. Throughout the history, inheritance is the most common.

---

## Social mobility in modern societies (at least in an idealized version)

![](./asset/20140201_USD001_1.jpg)

---

## The sociological explanation 社會學解釋

Sociologist Ralph Tuner proposed two types of social mobility 社會流動:

--

- .orange[Contest mobility] 競爭流動模式 is like a sporting event in which many compete for a few recognized prizes. Applied to mobility, the contest norms means that victory by a person of moderate intelligence accomplished through the use of .violet[common sense, enterprise, daring, and successful risk-taking] is more appreciated than victory by .violet[the most intelligent] or .violet[the best educated].

--

- .orange[Sponsored mobility] 庇護流動模式 ejects the pattern of the contest and favors a .violet[controlled] selection process. 

--
	+ In this process the elite or their agents, deem to be the best qualified to judge merit, choose individuals for elite status who have .violet[the appropriate qualities]. 
	+ Individual do not win or seize elite status; mobility is rather .violet[a process of sponsored induction to the elite].

???
Use the Bill Gates story last time for contest mobility.
Sponsored mobility in France and China, and Korea.

---

## Rising idea of .cyan[meritocracy]

Sponsored mobility model is closely linked with .cyan[meritocracy] 賢能主義/唯才主義, an idea that .orange[power/opportunity should be allocated based on ability and talent] (judged by pre-set rules):

- National elites design tests to select future talents

- Selective tests are considered most fair for all

--

Meritocracy is deeply rooted in Confucianism 儒家. Confucius himself cared deeply about how to design a system to pick the best suited to serve the country &lt;sup&gt;.red[4]&lt;/sup&gt;:

- This idea operates at societal level.

- It requires every participants to do whatever they can to gain an competitive edge in a society where the State controls most of the resources.

.footnote[[4] 李弘祺（2012）。.kt[學以為己: 傳統中國的教育。]香港中文大學出版社。]

---

## When combining meritocracy with a close-knit family culture

Your family's glory/destiny is tied to children's, as a result, families tend to .violet[_push_] the children very hard. 

The other side goes hand-in-hand: families also tend to invest tremendous amount of resources in their children.

At societal level, your social status is largely defined by: 1) the types of education received; 2) the relative status within each institution/department/major.

Your merit is judged by: .cyan[talent; GPA; internship; study abroad; awards, etc]. All these things are some versions of .violet[_competition_].

The competition is smallest at lower ranking institutions, but intensifies at the top of each level.
- .cyan[Can anyone reason why?]

???
Because the resources and social status associated with prestige of education is still a pyrimad shape: fewer and fewer elites at the very top.

But the premium associated with scarce degrees is maybe larger than ever.

---

class: middle, center

# The shadow of education is really a consequence when 
## .orange[1)] education becomes universal &amp; global; 
## .orange[2)] education is used as the sole indicator to judge/screen indivual success

---

class: inverse, middle

## Good or Bad? It's complicated

- Waste of resource / diminishing marginal returns
- The concern about inequality
  + Who has the resources to purchase better education?
  + At global level, shadow education favors emerging new global elite?
- But should we penalize parents who invest in the education of their children?

---

# .red[Summary]

Modern day competitive schooling is deeply rooted in culture, social relationships, and philosophy about individual &amp; society.

The shadow continues to grow: In a "school-ed society" (David Baker, Session 10), we are likely to see the spread of such education fever around the world.

- Not only getting more education
- But also getting "specialized" or "scarced" education

Is there a way out? It is possibly outside the education system itself: when we de-couple education &amp; social status, employment opportunity, etc.

- But if we don't use education, what will be the alternative?

---

## Resource:

- Turner, R. H. (1960). Sponsored and contest mobility and the school system. _American Sociological Review, 25_(6), pp. 855-867.

- Lin, L. (2022). *The Fruits of Opportunism: Noncompliance and the Evolution of China's Supplemental Education Industry*. Chicago, IL: University of Chicago Press. 
  + [My review of this book](https://journals.sagepub.com/doi/10.1177/20965311231167188)
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="https://yisuzhou.bitbucket.io/libs/plugin/wscale.js"></script>
<script src="https://platform.twitter.com/widgets.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
