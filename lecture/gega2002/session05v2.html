<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Education &amp; Globalization</title>
    <meta charset="utf-8" />
    <meta name="author" content="Dr. Zhou Yisu 周憶粟" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-fonts.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-extra.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: top, left, inverse, title-slide

.title[
# Education &amp; Globalization
]
.subtitle[
## Session 05
]
.author[
### Dr. Zhou Yisu 周憶粟
]
.date[
### 2023/08/31
]

---




# Two authors

.pull-left[
![:wscale 80%](https://cdn.britannica.com/90/185690-050-091C5B92/Dani-Rodrik-American.jpg)

Born 1957.8.14
]

.pull-right[
![](https://www.eastasiaforum.org/wp-content/uploads/2020/10/WGW_2.jpg)

Born 1930.10.9
]

???
So we have one economist, one historian, both offer a lot about globalization. Economists mainly concern the current situation. As you can see, Prof. Rodrik’s book is an illustration of analytical thinking at a grand scale. It traverses through world history, examining its implications on both economics and politics. Economists often liken countries to individuals - each pursuing its unique agenda (choices) while adapting to a host of conditions. These often include resource constraints, the available human labor pool, infrastructure standards, and access to global markets among others.

On the contrary, Prof. Wang, being a historian, is primarily focused on delving into the past. What captivates me about his work is how he writes from a distinctly Chinese perspective, emphasizing Chinese civilization as opposed to the nation state of China. This is of particular importance to us, as his writings enhance our understanding of our identity.

---

# Let's discuss the Rodrik book first

I want everyone to focus on Chapter 2 "The Rise and Fall of the first Globalization". It covers the period between, roughly, 1860-1914.

I have asked a volunteer to present their reading.

---

## Convention wisdom about globalization

What are the drivers?

- Technological advancements that reduce transaction costs
- Dominance of free trade ideology
- Adoption of the gold standard facilitates smoother capital flow

--

What's more? .orange[New institutions] (i.e. "structures" using our textbook definition)

- convergence in belief systems (economic liberalism and gold standard created more common belief)
- capitalism (and war)

---

# Globalization is not "new"

Except the "First Great Globalization", there are many other previous happenings of globalization. This is called "hardwired theory of globalization". 

--

Some researcher &lt;sup&gt;.red[1]&lt;/sup&gt; claims globalization stems from a .orange[basic human urge to seek a better and more fulfilling life].

Can be traced back to first movements of human species, part of early human history.

Has included activities of: trade 貿易, missionary work 傳教, adventures 冒險, and conquests 征服.

Therefore globalization is nothing new. It is hardwired into our species.

.footnote[[1] Chanda, Nayan. 2007. _Bound Together: How Traders, Preachers, Adventurers, and Warriors Shaped Globalization_. New Haven: Yale University Press.]

---

![](./asset/Reich2018-3.png)

.footnote[Reich, D., 2018. *Who we are and how we got here: Ancient DNA and the new science of the human past*. Oxford University Press. .kt[人类起源的故事]（2019）大卫·赖克（著），叶凯雄、胡正飞（译）。浙江人民出版社。]

???

There are at least multiple waves of large-scale human migration in our history.

Our ancestors interbreeed with Neanderthals; moving across the Africa continent; and reveal our genetic relatives.

After this period, different theorization.

---


### Global trade around 1300 CE

![:wscale 85%](./asset/Screenshot 2019-08-21 09.28.54.png)


.footnote[Major trade routes of Afro-Eurasia 1300 CE. Image credit: [The Cambridge World History](https://www.cambridge.org/core/books/cambridge-world-history/trade-and-commerce-across-afroeurasia/B866E0C4A8DB0316091A6ABF77410D59) But according to Hansen such routes existed even earlier.

Hansen, Valerie. *The Year 1000: When Explorers Connected the World—and Globalization Began*. Scribner, 2020. .kt[公元1000年。]（2021）韩森（著），刘云君（译）。读客。
]

???

The interaction intensified via trade, exploration, and conquer. Note, the "mutual", "two-way" nature of these activities.

Note also from Yang Bin's quote from last session.

---

# What is Prof. Rodrik's key argument

The foundation of the globalized world economy is unstable due to the incongruity of three elements:

.violet[The full and simultaneous coexistence of global economic integration, the nation-state, and democratic politics, is simply **impossible**.] This is called a trilemma.

--

Rodrik challenges the idea of a borderless world, arguing that hyperglobalization is neither feasible nor desirable. He labels this path undeniably disastrous. Instead, he suggests a shift of focus towards strengthening .cyan[domestic economies] and .cyan[democracies] to equitably distribute the benefits of globalization and to serve as a check on the global elite’s power.

To remedy this shaky foundation, Rodrik promotes **a balance between local autonomy and international cooperation**, as opposed to an all-encompassing global economic system.

---

## What is the reasoning behind Trilemma?

The Trilemma (an argument analogous to a dilemma but presenting three instead of two alternatives in the premises), suggests that a country cannot simultaneously pursue these three things - national sovereignty, democracy, and global economic integration. At best, it can accomplish only two out of the three.

---

1. If a country chooses national sovereignty and global economic integration, it must sacrifice democracy. .cyan[This is because global markets require the setting and enforcing of rules by institutions, and the nation's citizens (if it were democratic) may have different preferences than the mandates of the global market].

--

2. If a country chooses democracy and national sovereignty, the depth of its global economic integration will be limited. .cyan[The country may have policies reflecting the choice of local voters that do not align with the unrestricted flow of goods, services, and capital across borders.]

--

3. If a country decides to fully embrace democracy and global economic integration, it must give up a portion of its national sovereignty, .cyan[as global rules would limit the range of economic policies a country may want to implement.]

--

So the Trilemma exposes the inherent tension between these three aspects. The Logic behind it suggests that a **balance** needs to be struck, and complete globalization, as per Dani Rodrik’s perspective, may not fully suit the interests of individual nations, especially if they wish to preserve democratic processes and maintain certain degrees of sovereignty.

---

## The implication

For national leaders, achieving a balance among these three elements can be extremely challenging. If handled improperly, it can lead to a crisis:

- Financial globalization, such as the 1997 Asian financial crisis (Introduction, pp.x-xii)

  + A profound economic/financial integration occurred between Southeast Asian countries and the global capital market.
  + Countries like Thailand and Korea both had democratic governance.
  + This implies that they had to forfeit a portion of their sovereignty. Indeed, with foreign capital easily flowing in and out of these countries, profit-seeking entities can target their vulnerable economic systems.
  
--

.orange[Failing to address this paradox can lead to a backlash against globalization, as people become disillusioned with the perceived loss of control over their lives and the erosion of their social arrangements. ]

???

When I read it, I cannot help but recall what Marx once said: History repeats itself, first as tragedy, second as farce.歷史總是在重複自己，第一次是悲劇，再一次就變成了鬧劇

In chapter 2, we see this happen in 1920s.

---

## This paradox created winners and losers

This backlash can take the form of populist movements, which can threaten the stability of democratic institutions and lead to the rise of authoritarian leaders (i.e. Donald Trump). 

--

Central to the populist movement is a resentment towards the “winner-takes-all” economy. Who reaps the benefits from financial liberalization? When profit-seeking foreign capital takes flight, who bears the losses?

--

The author argues that addressing the paradox of globalization requires a rethinking of the relationship between markets and states, and a recognition that economic globalization must be balanced with social and political considerations.


&lt;!-- 

## Chapter 1 of Markets and States

Rodrik discusses the historical evolution of economic thought and the relationship between markets and states. The chapter contrasts the .violet[mercantilist view], which saw the state and commercial enterprise as serving each other's needs, with the .violet[economic liberal view], which believes that economies flourish when markets are left free of state control. 

The chapter argues that state-business collaboration (i.e. mercantilist) is a form of corruption and that protective barriers on trade reduce competition and harm economic growth. Overall, the chapter presents a critical view of mercantilism and argues in favor of economic liberalism.

 --&gt;


---

# Wang Gungwu

.pull-left[
![](./asset/wang_renewal1.jpeg)
]

.pull-right[
![](./asset/wang_renewal2.jpg)
]

---

## Why do I want you to read him?

For me, reading his work provides answers to some of the questions I’ve grappled with since I first moved abroad. I spent six years in the United States, and I’m returning next year. Many of you will also set off for different places after graduation. The question of “who we are” becomes more pertinent when exposed to a new environment.

--

The question then arises: .red[how do we define who we are]? Historians have pondered this question for centuries, often seeking answers through the construction of .cyan[narratives] — storytelling essentially. 

In this regard, Professor Wang’s book differs greatly from those of Professor Rodrik. Economists write with precision and harness the power of .cyan[analytics], while historians weave together threads from the past, drawing various analogies and consistently shifting the comparison context. Their narratives help us shape our perception of “self.”

---

### For instance, the Chinese society has endured lots of changes

We encountered the west, not in the most pleasant ways, to put it mildly.

Take, for instance, the First Opium War (1839-1842), a subject we studied in high school. We learned of the Qing Dynasty’s defeat by the UK, marked by unequal treaties favoring the western powers. These conflicts, coined as a “century of humiliation,” were painted as the birth of modern China — a transformative period, perplexing yet enlightening.

--

What would an economist say about this period? Let me quote Mr. Rodrik (p.139-140)


---

&gt;Those parts of the world which proved receptive to the forces of the Industrial Revolution shared two advantages. They had a large enough stock of relatively educated and skilled workers that could fill up and run the new factories. They also had sufficiently good institutions — well-functioning legal systems, stable politics, and restraints on expropriations by the state — to generate incentives for private investment and market expansion. With these pre-conditions, much of Continental Europe was ready to absorb the new production techniques developed and applied in Britain. Chalk up one for globalization.

--

&gt;Elsewhere, industrialization depended on “importing” skills and institutions. Intercontinental labor mobility was a tremendous advantage here. Where Europeans settled en masse, they brought with them both the skills and the drive for more representative, market-friendly institutions that would promote economic activity alongside their interests. .orange[The consequences were disastrous for the native populations], who perished in large numbers courtesy of European aggression and germs. But the regions of the world that the economic historian Angus Maddison has called “Western offshoots” — the United States, Canada, Australia, and New Zealand — were able to acquire the necessary prerequisites thanks to large immigrations. Supported also by sizable capital flows from Europe, these economies would eventually become part of the industrial “core.” Chalk up two for globalization.

---

&gt; Colonization’s impact on other parts of the world was quite different. When Europeans encountered inhospitable conditions that precluded their settlement in large numbers or began to exploit natural resources that required armies of manual workers, they set up institutions that were quite different from those in the Western offshoots. These purely “.orange[extractive]” institutions were designed to get the raw materials to the core as cheaply as possible. They entailed vast inequalities in wealth and power, with a narrow elite, typically white and European, dominating a vast number of natives or slaves. Colonies built on the extractive model did little to protect general property rights, support market development, or stimulate other kinds of economic activity. The plantation-based economies in the Caribbean and the mineral economies of Africa were typical examples. .orange[Studies by economists and economic historians have established that this early experience with institutional development — or lack thereof — has produced a debilitating effect on economies in Africa and Latin America that is still felt today]. Chalk up one against globalization.

---

### What would a historian say?

Professor Wang told us, saying the loss went beyond just military victories; .cyan[it was a changing of life itself]. The old worldview, “Tianxia” (p.6), offered a universal value system for those in search of progress towards civilization. It allowed us to progressively include foreign ideas into our sphere and civilization (Buddhism and Islam serve as examples of this). Despite this inclusivity, China always remained at the heart of this world order — the “middle kingdom” or “central state”. Yet, the historical China we knew was dramatically ruptured by contemporary principles brought by the West.

What is changed is not economic relationship. But also a value system we held true for hundreds of years.

--

For example, Chinese culture highly values “order” and “stability,” principles embedded in classical teachings like Confucianism. Western countries, however, champion freedom and individualism, but, significantly, they possess power. This pragmatic view compels us to ponder: are our long-held ways of life flawed? Did we follow the wrong political structure, the wrong worldview assuming our morals and civilization were universal?

---

### But my puzzle is far from an exception

Prof. Wang told to us that Chinese intellectuals, irrespective of their political beliefs, are all in search of a position within global society to establish their civilization.

--

He talked about change on p.6 (English version)

&gt;Change must occur when the existing way is no longer adequate, and only through change can a new workable way be found. As long as it works, the existing way lasts. Heaven protects those who change with the change of circumstances. .kt[易，穷则变，变则通，通则久，是以自天佑之，吉无不利。]

---


There is, however, a problem — our familiarity with the orthodox history we were taught growing up. 

Unfortunately, this conventional approach did not satisfy my curiosity or answer my questions. I’m referring not to specific details — like who did what, where — but to broader inquiries: .violet[where we originate from, where we are now, and where we’re headed].

--

I believe all histories hold value. Yet, what’s equally crucial is not limiting ourselves to one interpretation or even one language. Anthropologists recommend “making the familiar strange” as a strategy to deepen our comprehension. For example, reading Chinese history written by someone outside of China can provide fresh insights. This is why I bring you this book.

--

Furthermore, Professor Wang suggests that the task is not only about crafting a more accurate history but connecting twentieth-century Chinese history to the imperial past. Doing so, he contends, will clarify both disruptions and continuities.

---

# The book's main argument

In "Renewal," Prof. Wang explores the concept of the Chinese nation-state in the context of global history. Unlike historical Western nation-states that were typically defined by ethnicity or language, the Chinese state was formed largely from a .violet[civilizational perspective] rooted in ideals of .violet[cultural unity] and .violet[shared history].

--

Prof. Wang reviews China's historical trajectory that led to the development of this unique nation-state. He delves into how China, over the centuries, has been able to maintain cultural continuity while adapting to the shifting forces of globalization. He discusses China's renewal strategies and statesmanship that ensured its resilience and endurance through the rise and fall of dynasties, foreign invasions, and modernization challenges.

--

Prof. Wang also seeks to understand China's high sense of civilizational pride, arguing that this pride could be both a .orange[force] that drives the nation towards global leadership and a .orange[barrier] to interacting smoothly with the global community.

---

## How does Wang Gungwu's perspective on the Chinese state and its history differ from other scholars in the field?

The essays collected in this work do not claim to be comprehensive in capturing the debates that are going on. Wang Gungwu's perspective on the Chinese state and its history is drawn from .violet[the point of view of an ethnic Chinese who has looked at the subject largely from the outside]. 

--

He has attempted to understand China's efforts to be a modern state and civilization by looking at .violet[interpretations of history] for clues as to how the Chinese deal with issues like building a nation after having been an empire for 2,000 years, and also how they are trying to recover their distinctive cultural heritage through embracing modernity.


&lt;!-- ### How has the concept of the Chinese state changed over time, and what factors have influenced these changes?

The concept of the Chinese state has evolved over time, with different ideas and institutions emerging and fading away. One of the most relevant concepts for China's modern history is the act of *tianxia yitong*, unification by the central bureaucratic state established by Qin Shihuang in 212 BC. From then to 1912, this imperial system became a key concept in China's evolving civilization, the idea that unification was the norm and division an aberration. Another concept was the earlier concept of Zhongguo, from its symbolic centrality during the Shang and Zhou dynasties to the territorial state of the Qin-Han dynasties. This central state waxed and waned thereafter but it became increasingly powerful during the Ming and Qing dynasties and reached its acme with the PRC. 

### In what ways has China's relationship with other nations and global powers shaped its development as a state?

Joining the League of Nations did not save China from losing control of (Outer) Mongolia and Manchuria. Nor did it prevent another war with Japan that China had little hope of winning. Fortunately, the United States entered the war and after 1945 catapulted China to Great Power status by insisting on having the Republic of China as one of the five permanent members of the Security Council of the newly established UN. This provided the cockpit from which China could try to re-position itself. Ever since 1978, the PRC has systematically laid the foundation for its Great Power status through massive economic reforms. Now that the reforms have proved successful and have had their dramatic impact on the capitalist world, how has this affected Chinese relations with its neighbours in Asia? Chinese history shows that when the country was disunited and weak, like the Nationalists' Nanjing government, it had neither family nor friend among its neighbours. In any case, its neighbours were subject to foreign dominance (including Thailand, even though it was formally independent) or were actually under colonial administration. Japan was the exception but it had become a Great Power. 

--&gt;

---

## The changes and constant are what attracts me as a reader

### about statehood

&gt;Whatever the changes, the goal for Chinese remained the same for millennia. .violet[They wanted a powerful state to keep the large country together.] They saw the need for strong leaders, and it mattered less who they were and where they came from than whether they could bring them stability, and better still, if they could support the enrichment of China's civilization, the key to the cohesive strength of the Chinese as a people. 

&gt;.kt[不管发生什么变化，中国人的目标几千年来始终不变。他们需要一个强有力的政府把这么大的国家统一起来。他们看到了强势领导者的必要性，至于这样的领导者是谁，来自何处，这并不重要。重要的是他们能否带来稳定，以及能否保证中国文明的繁荣发展，因为这是将中国人凝聚起来的关键。]

---

### about order and center

&gt;If kingdoms and empires fought endlessly without some vision of an ideal order, the results would undermine faith in any kind of universalism. .violet[The Chinese saw the need for a stable centre. They conceived of the idea of Zhongguo ("central state") as a political order that also served as the source of moral authority.] Petty states, kingdoms, and even empires, could fight among themselves around that central state, but if they conformed to the values the central state represented, they were on the side of history and advancing in the right direction. Thus, for many Chinese thinkers history demonstrates that China has played a valuable role in helping to civilize the world. (p.9-10)

&gt;.kt[如果某些王朝和帝国因缺乏对理想秩序的认同而征战不已，结果就会破坏对任何一种普遍主义的信仰。中国人看到了一个稳定的中心的必要性。他们将中国（中央之国）的概念设想为一种政治秩序，这种政治秩序同时也成为道德权威的源泉。小规模的国家、王国甚至帝国会在中央国家周围相互征战，但是如果他们遵从中央国家所代表的价值，他们就是顺应历史的，是按照正确方向发展的。因此，对很多中国思想家来说，历史证明中国在帮助世界文明化的过程中起过重要的作用。]

---

## A question for your consideration

What priority will China assign to the three factors in Rodrik's trilemma?

- national sovereignty, 
- democracy, 
- global economic integration.

---

## The study of globalization let us confront our ambivalence

.pull-left[
.font80[Modern China has been .violet[torn between] affirming the integrity of its history, despite the Sinocentric bias, and accepting world history in the Eurocentric tradition. This ambivalence has not helped other people understand China's position in world history. Why this ambivalence? China obviously does not wish to accept a narrative that reads history backwards to take European history since the seventeenth and eighteenth centuries as the norm. .violet[Together with other peoples with a strong heritage of their past, the Chinese think it is un-historical to impose one dominant narrative upon everyone's past. What is universal is not necessarily what the most powerful people at any one time can dictate to all other peoples.] ]

]

.pull-right[
.kt[现代中国一直左右为难：一方面是坚持其历史的统一性，虽然带有中华中心主义的偏见；另一方面是接受带有欧洲中心主义传统的世界历史。这样一种游移不定的状态并不利于别人理解中国在世界历史中的地位。那么为什么会有这种摇摆不定呢？中国显然并不希望接受一种以十七、十八世纪欧洲历史为标准的历史解读话语。像其他拥有强大历史传统的人们一样，中国人认为把一种主导话语强加给每个人的过去，这种做法是非历史的。普遍的东西并不必然是强者在任何时候都可以用来命令别人服从的。]

]
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="https://yisuzhou.bitbucket.io/libs/plugin/wscale.js"></script>
<script>var slideshow = remark.create({
"ratio": "4:3",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
