<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>Education &amp; Globalization</title>
    <meta charset="utf-8" />
    <meta name="author" content="Dr. Zhou Yisu 周憶粟" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-fonts.css" type="text/css" />
    <link rel="stylesheet" href="https://yisuzhou.bitbucket.io/libs/remark-css/robot-extra.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: top, left, inverse, title-slide

.title[
# Education &amp; Globalization
]
.subtitle[
## Session 12
]
.author[
### Dr. Zhou Yisu 周憶粟
]
.date[
### 2023/09/25
]

---





# Global flows of people

## Aims
To consider different types of global flows of people

To examine the .orange[push] and .orange[pull] factors involved in shaping flows of people

To examine the structures that expedite or impede (i.e. barriers) flows of people

---

## A humorous tweet


.pull-left[
.green[Original:] 「每年十月一日，又到了中国潜水员交配的季节。他们不远万里，成群结队的来到东南亚各珊瑚礁海域，觅食、产卵。这是一趟非凡的旅程，他们将冒着被海洋大型掠食者捕杀、海洋船只螺旋桨、以及海洋内不稳定洋流的危险，进行为期7天的交配。然后再各自返回他们的栖息地。」
]

.pull-right[ .green[Translation:] Every October 1st is the mating season of Chinese divers. They traveled thousands of miles in teams and groups to the South East Asian coral reefs for food and mating. This is an extraordinary journey: These divers risked their lives of being eaten by large sea predators, strangled by ship propellers, and carried away by unstable ocean currents. They mated during these 7 days and returned to their habitats.]

???
Today this sounds more like a joke. But quite real just a couple of years back.

---
## People flow

Globalization can be seen to have .cyan[expedited] (i.e. speed up) global flows of people, although there is a long history of exodus and travel throughout the world.
- nation-state's strong governing power (classifying, documenting, facilitating, mobilizing, etc).
- short-term pressure: skilled labors (e.g. STEM) cannot be produced overnight.
- long-term pressure: demographic pressure in aging societies.
- redemption for colonial history (e.g. UK, Portugall)
- changing attitudes toward refugees (e.g. Syria; central America)

--

Different flows: 
- .orange[Migrants/ Immigration] 移民: People who live in a country where they were not born and have important social ties to that country
- .orange[Human trafficking survivors] 人口販運
- .orange[Tourists] 遊客

---

background-image: url(./asset/migration-core-periphery.png)
background-size: contain

???
- prior to the 20th century, there are much freedom of movement.
- example: about 50 million people left Europe for the U.S. between 1820 and 1900. Before 1880, entry into the U.S. is largely unregulated


---

## Migration/Immigration

.pull-left[

.brown[Includes:]
- Temporary labor migrants 臨時性勞工移民
- Highly skilled migrants 高技術移民
- Forced migrants 強制移民
- Family reunification migrants 家庭團聚
- Return migrants 返鄉移民 

]

.pull-right[

.brown[Problems involved in tracking such flows:]
- Lack of or inadequate data collection
- Use of different definitions and measures
- Little tracking of expats &lt;sup&gt;.red[1]&lt;/sup&gt;
- Lack of information on undocumented immigration

.footnote[ [1] expats is short for .violet[expatriate], meaning "a person who lives outside their native country". The term often refers to a professional or skilled worker who intends to return to their country of origin.]

]

???
We have different types of migration. But to count them is the most difficult part.

---

## Example: Macao as a "floating city"

.pull-left[
.green[English:] .violet[Total population] of Macao comprises .violet[local population] as well as .violet[non-resident workers] and .violet[non-local students] living in Macao. Total population was 682,070 in August 2021, an increase of 129,567 or 23.5% compared to August 2011. The average annual growth over the past ten years was 2.1%, a slowdown of 0.3 percentage points from the 2.4% growth during 2001-2011.

- Local population: 568,662 (83.4%)
- None-resident workers: 94,637 (13.9%)
- Non-local students: 18,771 (2.8%)

]

.pull-right[
.green[中文:] .violet[澳門人口]包括.violet[本地人口]以及在澳居住的.violet[外地僱員]和.violet[外地學生]。2021年8月本澳.violet[總人口]共682,070人，較2011年 8月增加129,567人，增幅為23.5%；過去十年的年平均增長率為2.1%，較2001至2011年間的2.4%減少0.3個百分點，增長速度有所放緩。

- 本地人口：568,662（83.4%）
- 外地僱員：94,637（13.9%）
- 外地學生：18,771（2.8%）
]


.footnote[Souce: https://www.dsec.gov.mo/en-US/Statistic?id=103]

???

.pull-left[
.green[English:] .violet[Total population] of Macao was 650,834 in August 2016, an increase of 98,331 or 17.8% as compared with 2011. Population size increased significantly as against 414,128 in 1996. During the past 20 years, the most rapid population growth was observed between 2011 and 2016, up by 17.8% in five years (an average annual growth rate of 3.3%). This was mainly due to a substantial increase in .violet[non-resident workers] living in Macao and a rebound in the .violet[birth rate] over the past five years.
]

.pull-right[
.green[中文:] 2016年8月澳門.violet[總人口]有650,834人，較2011年增加98,331人，增幅為17.8%。對比1996年(414,128 人)人口數量明顯上升;過去二十年，人口增長以2011年至2016年間最 為迅速，五年間增加了 17.8%(年平均增長率為3.3%)，這主要是最近五年居澳 .violet[外地僱員]大幅增加以及.violet[出生率]回升所致。
]

---

## Continue: Macao as a "floating city"

.pull-left[
.green[English:] Non-resident workers and non-local students living in Macao rose by .violet[51.9%] and .violet[279.7%] respectively compared to 2011, with their respective proportions in the total population growing by 2.6 percentage points and 1.9 percentage points to 13.9% and 2.8%. Besides, the local population showed an increase of .violet[17.2%] over the past decade; yet, its proportion in the total population dropped by 4.4 percentage points to 83.4% as the growth of the local population was smaller than the increases in non-resident workers and non-local students living in Macao.
]

.pull-right[
.green[中文:] 與2011年比較，在澳居住的外地僱員及外地學生分別增加了.violet[51.9%]及.violet[279.7%]，佔總人口的比例為13.9%及2.8%，增幅分別為2.6個及1.9個百分點；另一方面，本地人口較十年前增加了.violet[17.2%]，由於其增幅不及外地僱員及外地學生，故在總人口中的佔比較2011年減少4.4個百分點至83.4%。

]

---

background-image: url(./asset/macgw2022.png)
background-size: contain

### Migrant worker population in Macao

???
Why more female workers during 1998-2003? and more male workers since then?

---

### The nature of population flow

Macao's population has a complex structure. Even among residents, workers and students, there are those who live in Macao and those who don't.

--

- How to identify .cyan[non-resident workers] (外地僱員)? This is a highly heterogenous group:
	+ body workers such as those in construction and domestic helpers
	+ out-soucers such as those in telecommunication
	+ mid- to high-level management in Casinos and universities.

--

To correctly account for the moving population is difficult:

- What about those who .cyan[move across border everyday]?
- What about those .cyan[Macao residents who live in Zhuhai]? 
	+ In 2022, about .violet[32,000] Macao citizens lived in Zhuhai or neighboring areas
	+ Among them, everyday there were about .violet[4,300] students commute to Macao for school.

--

The bottom line is: .red[human popolulation has certain "fluid &amp; dynamic" nature to it]. When we want to specify the flow of people under the nation state framework at a particular point of time, we immediately encounter issues.

???
Macao is not an exception. But falls in line with other global cities (Singapore, Dubai, etc)
---

## The push/pull framework
is a simple, static framework to understand the flow of people

.orange[Push] factors might include:

- Economic depression 經濟不景氣
- Political persecution 政治迫害
- War 戰爭
- Famine 饑荒

.orange[Pull] factors might include:

- Favorable immigration policies 更優厚的移民政策
- Labor shortage 勞工短缺
- Similarities in culture and language 文化、語言的相同性 (i.e. colonial linkage)
- Social networks (i.e. 同鄉會 an association of fellow provincials or townsmen)

---

### Numerous migration policies, including facilities or restrictions, might be in place, including:

- The wish to retain labor in order for the host region to remain competitive in global markets
- However, an increase in immigrant populations can lead to local disputes
- Fears of terrorism often encourage or justify stringent restrictions on migration (these days, this could be attributable to COVID)
- The dilution of cultural identity 對身份認同的稀釋

Closely related to role of nation-state: .violet[limited &amp; bounded, identifying "otherness"]

---

## .cyan[Analysis:] Singapore's immigration policy

From 2000-17, its population grew from roughly 4 mil to 5.6 mil. Since 2017, that number stayed stable. That growth is driven by: .violet[PR] (permanent residents) &amp; .violet[Non-residents] (mostly workers)

![](./asset/demographics_chart_1-940x523.jpg)

---

### Singapore's population in 2022

![:wscale 85%](./asset/diagram-1-total-population-june-2022.jpg)

&lt;!-- singapore population breakdown --&gt;

???

27% is non-residents.

CMP = construction, marine, process

The S-Pass allows mid-level skilled staff to work in Singapore. Candidates need to earn at least $2,500 (MOP14,700)a month and have the relevant qualifications and work experience.

---

## .green[Analysis:] Singapore's immigration policy

__Case:__
- English version: http://bit.ly/2oEHxPw
- Chinese version (slightly more detailed): 
	+ https://zhuanlan.zhihu.com/p/79782875
	+ OR https://www.spacious.hk/zh-tw/blog/%E6%96%B0%E5%8A%A0%E5%9D%A1%E7%A7%BB%E6%B0%91%E6%87%B6%E4%BA%BA%E5%8C%85/

__Questions to consider:__
- Is immigration for everyeone? Why Singapore allows immigration?
- What are the benefits?
- What are the push &amp; pull factors?

---

## .orange[Push factors]: Why people want to leave their hometown

--

- Not satisfied with current living conditions:

	+ Limited Living space
	+ Unaffordable Housing
	+ Polluted environment
	+ Bad infrastructure

--

- Bad job markets (e.g. low pay &amp; toxic working environment)

--

- Bad governance (corruption, etc)

--

- No Rule of law (safety)

--

- Hoping for a better education for children

--

- Paying too much taxes without getting much benefits

--

- Identify with foreign culture

---

## The appeal of a developed nation

&lt;iframe width="800" height="575" frameborder="0" scrolling="no" marginwidth="0" marginheight="0" src="https://www.google.com/publicdata/embed?ds=d5bncppjof8f9_&amp;amp;ctype=l&amp;amp;strail=false&amp;amp;bcs=d&amp;amp;nselm=h&amp;amp;met_y=ny_gdp_pcap_kd&amp;amp;scale_y=lin&amp;amp;ind_y=false&amp;amp;rdim=region&amp;amp;idim=country:SGP:IND:CHN:MYS:IDN&amp;amp;ifdim=region&amp;amp;hl=en_US&amp;amp;dl=en_US&amp;amp;ind=false&amp;amp;xMax=180&amp;amp;xMin=-180&amp;amp;yMax=-80.12035963519642&amp;amp;yMin=86.50998070952664&amp;amp;mapType=t&amp;amp;icfg&amp;amp;iconSize=0.5"&gt;&lt;/iframe&gt;


---

## .orange[Pull factors]: Why Singapore wants more people

- .violet[Aging population] 人口老齡化: shortage of young people

- .violet[Skills shortage] 勞動技能短缺: highly skilled, entrepreneurial workforce

- .violet[Capital shortage] 資本短缺: seeking external investment or capable business man to boost economy

--

Therefore Singapore aims to pull in immigrants with these attractions:

- Social, economic, political benefits
	+ Highly efficient governance
	+ Less corruption
	+ Lower taxes (Tax rate on capital gains is 0%)
- Ease of travel (Singapore's passport ranked No. 4 globally https://www.passportindex.org/byRank.php)
- Geographic location: intersection of China, India, Southeast Asia.

---

### Singapore: we need .red[young] people

.pull-left[
![](./asset/age-singapore1990.png)
]

.pull-right[
![](./asset/age-singapore2020.png)
]

---

### Singapore: we need .red[skills]

.pull-left[
Skills at all levels: 

- lower-end body workers
- mid-level workers (of all kinds)
- higher-end knowledge workers.

- students: future knowledge workers
	+ Stay two or more years in Singapore
	+ Study at a Singporean institution
	+ Pass a national-level test

]

.pull-right[
![](https://cdn.spacious.hk/zh-tw/blog/wp-content/uploads/2023/04/INFO-singapore-new-1-768x858.jpg)
]

---

### Singapore: we need .red[capital]

![](./asset/Screenshot 2019-10-03 16.08.32.png)

.footnote[Source: http://str.sg/JZBZ]

---

### A parallel process: "Low-end globalization" 低端全球化 by Gordon Mathews

.pull-left[
![](https://www.hongkongfp.com/wp-content/uploads/2017/06/gordon-mathews_d.jpg)

- mostly small merchants, construction workers, traders..
]

.pull-right[
![](https://www.hongkongfp.com/wp-content/uploads/2017/12/000_Hkg9003972.jpg)

- nation-states are ".violet[porous]" (i.e. having tiny spaces or holes through which liquid or air may pass)

]

---

class: .my-one-page-font

.pull-left[
.green[English:] When people in the developed world think about globalization, what typically comes to mind are the goods and services of multinational corporations—McDonald’s, Coca-Cola, Apple, Samsung, Sony, Facebook, Google, and so on. This is what I term “.violet[high-end globalization]” — globalization as typically practiced by vast organizations with billions of dollars in budgets, planet-wide advertising campaigns, and battalions of lawyers. Aside from this, there is another kind of globalization, “.violet[low-end globalization],” “.brown[the transnational flow of people and goods involving relatively small amounts of capital and informal, sometimes semi-legal or illegal transactions, often associated with ‘the developing world’ but in fact apparent across the globe.]” This is globalization as experienced by most of the world’s people...
]

.pull-right[
.green[中文:] 發達國家的人想到全球化，腦海裏一般會浮現跨國企業的產品和服務，如麥當勞、可口可樂、蘋果、三星、索尼、臉書、谷歌等。我稱這些爲「.violet[高端全球化]」，即由大型機構通過數十億元財政規劃、全球廣告宣傳及雄厚的律師隊伍來典型實施的全球化。在此以外還有另一種全球化，名爲「.violet[低端全球化]」。這是指「.brown[人和產品在較少資本運作下的非正式跨國流動，有時牽涉到半非法或非法的交易行爲，往往與『發展中國家』相關，但在全球都顯著可見]」。這是世界上多數人所經歷的全球化，它由那些有一些親朋好友的商人運作。他們購買較小數量的產品，常常通過街頭小販或路邊店鋪售賣給顧客，而非大型購物商場或商店。
]

---

## Dynamics of migration
.violet[Remittances] 匯款: accounts for significant share of sending countries' economy


.violet[Diaspora] 移民社群: 
- The Turkish case. There are about 1.5 million people living in Germany holding Turkish citizenship. They represented very important political resources &lt;sup&gt;.red[2]&lt;/sup&gt;

Because many immigrant are entrepreneurs, scientists, social innovators, policy makers have been studying: .cyan[Brain drain] and .cyan[Brain gain] 人材外流 &amp; 人材聚集. 

National government (such as Singapore) particularly concerns about attracting talented people:
- "Brains are everywhere"

.footnote[ [2] For more explanations: http://www.politico.eu/article/turks-abroad-play-pivotal-referendum-role-netherlands-erdogan-diaspora/]

---

## Forced migration

.violet[Asylum seekers] 政治庇護: refugees who seek to remain in the country to which they flee.


.violet[Refugees] 難民: those forced to leave their homeland, or who leave involuntarily, because they fear for their safety.

Example: 

- Syria refugee crisis: https://www.lucify.com/the-flow-towards-europe/
- Central America refugee crisis: https://www.unrefugees.org/emergencies/central-america/



---

## Human trafficking

&gt; The recruitment and movement of people through force or coercion, for purposes of sexual exploitation or forced labor


700,000–2 million people trafficked/year

Has increased dramatically since early 2000s

Coordinated through transnational criminal networks

Highly profitable "industry"

---

### Sex trafficking:
- Victims used as sexual slaves or sold to pimp
- Most common recruitment method is through deceitful job offer
- Mostly women and girls, but also boys
- Various tactics used to keep victims from running away


### Labor trafficking
- Forced labor
	+ domestic workers (see next slide)
	+ fisherman ([a case of Taiwan](https://www.twreporter.org/topics/far-sea-fishing-investigative-report))
- Interactive table: http://got-data.blogspot.com/2014/03/cost-of-human-life-and-human.html

---

background-image: url(./asset/lola1920.jpg)
background-size: contain
background-position: top

### Further read: An intimate story about immigration

.footnote[
Synopsis: 

她與我們家人一起生活了五十六年。她帶大了我和我的兄弟姐妹，從早到晚做飯做清潔，從來沒有工資。十一歲的我，一個典型的美國孩子，意識到她其實是我們家的奴隸。


She lived with us for 56 years. She raised me and my siblings without pay. I was 11, a typical American kid, before I realized who she was. 

]

---

# .red[Summary]

Global people flows are not new but increasing and becoming more complex, multidirectional


Whilst barriers impact flows of migrants, low-skilled migrants are most significantly impeded (i.e. "we want you but we don't want you forever")


Nation-states have a significant role to play (not just in terms of policy, but also identity)


Flows of people have generally been treated very differently to political, economic, and technical flows


Many barriers remain, some increasing, depending on type and direction of the flow of people 


---

## Resources

- Content is taken from Rizter &amp; Dean, Chapter 10
&lt;!-- - About identity
    + What is political tribes? https://law.yale.edu/yls-today/news/professor-amy-chua-publishes-book-political-tribes
    + A middle ground? [Why anyone can be Chinese by Daniel Bell](https://www.wsj.com/articles/can-anyone-be-chinese-1500045078) --&gt;
- Gordon Mathews interview
	+ English: http://bit.ly/2y6jj1S
	+ Chinese: http://bit.ly/2OrCGMW
	+ His book: _The World in Guangzhou: Africans and Other Foreigners in South China’s Global Marketplace_ 世界在廣州：南中國全球貿易市場中的非洲人和其他外國人
- My family's slave: Lola's story
	+ English version: https://www.theatlantic.com/magazine/archive/2017/06/lolas-story/524490/
	+ Chinese version: https://mp.weixin.qq.com/s/d7_ygIwiabQfNbnLxQtgkg
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script src="https://yisuzhou.bitbucket.io/libs/plugin/wscale.js"></script>
<script src="https://platform.twitter.com/widgets.js"></script>
<script src="https://www.lucify.com/embed/the-flow-towards-europe-updating/resize.js"></script>
<script>var slideshow = remark.create({
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
